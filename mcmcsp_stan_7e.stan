data {
  int<lower=0> npix ;
  int<lower=0> nages ;
  int<lower=0> nfehs ;
  int<lower=0> nRv;
  vector[npix] yobs ;
  matrix[nages*nfehs*nRv,npix] ymod ;
}
parameters {
  real<lower=1.,upper=nages> age ;
  real<lower=1.,upper=nfehs> feh ;
  real<lower=1.,upper=nRv> Rv ;
  real<lower=0.> erry ;
}
model{
  vector[npix] yinterp ;
  vector[npix] yinterp1 ;
  vector[npix] yinterp2 ;
  vector[npix] yinterp3 ;
  vector[npix] yinterp4 ;
  vector[npix] yinterp5 ;
  vector[npix] yinterp6 ;
  real weiga1 ;
  real weiga2 ;
  real weigRv1 ;
  real weigRv2 ;
  real weigz1 ;
  real weigz2 ;
  int ia2 ;
  int iz2 ;
  int iRv2;
  int j1 ;
  int j2 ;
  int k1 ;
  int k2 ;
  int l1;
  int l2;
  int m1;
  int m2;
  erry ~ normal(0.,1.) ;

  ia2 = 1 ;
  while (ia2 < age)
    ia2 = ia2 + 1 ;
  weiga1 = ia2 - age ;
  weiga2 = 1. - weiga1 ;
  
  iz2 = 1 ;
  while (iz2 < feh)
    iz2 = iz2 + 1 ;
  weigz1 = iz2 - feh ;
  weigz2 = 1. - weigz1 ;
  
  iRv2 = 1 ;
  while (iRv2 < Rv)
    iRv2 = iRv2 + 1 ;
  weigRv1 = iRv2 - Rv ;	
  weigRv2 = 1. - weigRv1 ;  
	


  j1 = (iz2 - 2)* nages + ia2 - 1+ (iRv2-2)*nfehs*nages ;
  j2 = j1 + 1 ;
  yinterp1 = (ymod[j1]*weiga1 + ymod[j2]*weiga2)' ;
  k1 = (iz2 - 2) * nages + ia2 - 1+ (iRv2-1)*nfehs*nages ;
  k2 = k1 + 1 ;
  yinterp2 = (ymod[k1]*weiga1 + ymod[k2]*weiga2)' ;
  l1 = (iz2 - 1) * nages + ia2 - 1+ (iRv2-2)*nfehs*nages ; 
  l2 = j1 + 1 ;
  yinterp3 = (ymod[l1]*weiga1 + ymod[l2]*weiga2)' ;
  m1 = (iz2 - 1) * nages + ia2 - 1+ (iRv2-1)*nfehs*nages ; 
  m2 = m1 + 1 ;
  yinterp4 = (ymod[m1]*weiga1 + ymod[m2]*weiga2)' ;
  yinterp5 = yinterp1*weigRv1 + yinterp2* weigRv2;
  yinterp6 =  yinterp3*weigRv1 + yinterp4* weigRv2;

  yinterp = yinterp5*weigz1 + yinterp6*weigz2;
  
  yobs ~ normal(yinterp, erry) ;
}
