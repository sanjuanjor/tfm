from __future__ import division 
from __future__ import print_function
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.stats import norm, beta
import pickle
from pystan import StanModel
from astropy.io import fits
from fjg import *
import mcmcsp_fun as mcsp
import pyphot
from graphics import spectrums

itypef = "8"
itypemod ="4"
cerrob = "N"
bin =10

emiles = spectrums()
emiles.itypef = itypef
emiles.itypemod = itypemod
emiles.cerrob = cerrob
emiles.load_spectra(bin)
emiles.create_model("a.ref")
emiles.load_observation("a.ref")
chains = 4  # 3
warmup = 1500  # 500
niter = 9500  # 9500
thin = 1
n_jobs = 4
stanparameters = {'chains': chains, 'warmup': warmup,'niter': niter, 'thin': thin, 'n_jobs': n_jobs}

#normal just testing
emiles.Rv=[2.0,2.5,3.,3.5,4.0,4.5,5.0]
emiles.atenuation(emiles.Rv)
#emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the libraryp

#for k in range(emiles.nages*emiles.nfehs*len(emiles.Rv)):
#    summodels = sum(emiles.modelsb[k, ])
#    emiles.modelsb[k, ] = emiles.modelsb[k, ] / summodels * emiles.infilt

data_stan = emiles.data_stan()
dataforstan = data_stan["1"]
modelfile = data_stan["2"]
fitfile = data_stan["3"]

#emiles.redshift(0.5)

emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters)
