data {
  int<lower=0> npix ;
  int<lower=0> nages ;
  vector[npix] yobs ;
  vector[npix] yerr ;
  matrix[nages,npix] ymod ;
  vector[nages+1] limage ;
  real tmin ;
  real tmax ;
}
parameters {
  real<lower=tmin,upper=tmax> t ;
  real<lower=0.> tau ;
  real<lower=0.> erry ;
  vector[npix] yreal ;
}
model{
  real suma ;
  real wei ;
  real ta ;
  real tb ;
  vector[npix] yinterp ;
  
  erry ~ normal(0.,1.) ;
  tau ~ normal(0.,15.) ;

  for (j in 1:npix) {
    yinterp[j] = 0. ;
  }
  for (i in 1:nages) {
      ta = fmax(0., t-limage[i+1]) ;
      tb = fmax(0., t-limage[i]) ;
      wei = tau*((ta+tau)*exp(-ta/tau) - (tb+tau)*exp(-tb/tau)) ;
      yinterp = yinterp + (ymod[i])' * wei ;
  }
  suma = sum(yinterp) ;
  yinterp = yinterp/suma*npix ;

  yreal ~ normal(yinterp, erry) ;
  yobs ~ normal(yreal, yerr) ;
}
