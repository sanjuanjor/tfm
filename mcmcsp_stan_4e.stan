data {
  int<lower=0> npix ;
  int<lower=0> nages ;
  int<lower=0> nfehs ;

  vector[npix] yobs ;
  matrix[nages*nfehs,npix] ymod ;
  vector[nages] xages ;
  vector[nages] incage ;
}
parameters {
  real<lower=0.,upper=1.> mu ;
  real<lower=0.> theta ;
  real<lower=1.,upper=nfehs> feh ;
  real<lower=0.> erry ;
}
transformed parameters {
  real<lower=0.> a ;
  real<lower=0.> b ;
  a = theta * mu ;
  b = theta * (1. - mu) ;
}
model{
  int iz2 ;
  int j1 ;
  int j2 ;
  real weigz1 ;
  real weigz2 ;
  real suma ;
  vector[nages] sfh ;
  vector[npix] yinterp ;
  vector[npix] yinterp1 ;
  vector[npix] yinterp2 ;
  
  erry ~ normal(0.,1.) ;
  theta ~ normal(0.,50.) ;

  iz2 = 1 ;
  while (iz2 < feh)
    iz2 = iz2 + 1 ;
  weigz1 = iz2 - feh ;
  weigz2 = 1. - weigz1 ;

  for (i in 1:nages) {
      sfh[i] = pow(xages[i], a-1.) * pow(1.-xages[i], b-1.) ;
  }

  for (j in 1:npix) {
    yinterp1[j] = 0. ;
    yinterp2[j] = 0. ;
  }

  j1 = (iz2 - 2) * nages ;
  j2 = (iz2 - 1) * nages ;
  for (i in 1:nages) {
      yinterp1 = yinterp1 + (ymod[j1+i])' * sfh[i]*incage[i];
      yinterp2 = yinterp2 + (ymod[j2+i])' * sfh[i]*incage[i];
  }
  yinterp = yinterp1*weigz1 + yinterp2*weigz2 ;

  suma = sum(yinterp) ;
  yinterp = yinterp/suma*npix;

  yobs ~ normal(yinterp, erry) ;
}
