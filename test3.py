from __future__ import division 
from __future__ import print_function
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.stats import norm, beta
import pickle
from pystan import StanModel
from astropy.io import fits
from fjg import *
import mcmcsp_fun as mcsp
import pyphot
from graphics import spectrums
AGE=10.
itypef = "1"
cerrob = "N"
bin = 10
itypemod = "5"

print('Type of models:')
if itypemod =="1":
    print('    1 = MILES')
elif itypemod == "2":
    print('    2 = Bruzual (9 bands)')
elif itypemod == "3":
    print('    3 = Bulges (Gorgas & Jablonka) spectra')
elif itypemod == "4":
    print('    4 = EMILES')
elif itypemod == "5":
    print('    5=JPAS_EMILES')
#itypemod = raw_input('? ')
print('Type of fit:')
if itypef == "1":
    print('    1 = single burst (only age, solar Z)')
if itypef == "2":
    print('    2 = single burst (age and metallicity)')
if itypef == "3":
    print('    3 = beta SFH (solar Z)')
if itypef == "4":
    print('    4 = beta SFH (variable Z)')
if itypef == "5":
    print('    5 = delayed exponential SFH (solar Z)')
if itypef == "6":
    print('    6 = delayed exponential SFH (variable Z)')
# itypef = raw_input('? ')
# bin = int(raw_input('Bin? (0=none) '))
# cerrorb = raw_input('Individual error bars (Y/N)? ')
print("Individual error bars",cerrob)
print("Bin =",bin)
emiles = spectrums()
emiles.itypef = itypef
emiles.itypemod = itypemod
emiles.cerrob = cerrob
emiles.load_spectra(bin,0)
ageyr=AGE
mfeh=0
a=0
b=0
t0yr=0
tauyr=0
error=0.02
parameters = {'ageyr': ageyr, 'mfeh': mfeh,'a': a, 'b': b, 't0yr': t0yr,'tauyr':tauyr,"error":error}
emiles.create_model("a.ref",parameters)
emiles.load_observation("a.ref")
chains = 4  # 3
warmup =1500  # 500
niter = 9500  # 9500
thin = 1
n_jobs =4
stanparameters = {'chains': chains, 'warmup': warmup,'niter': niter, 'thin': thin, 'n_jobs': n_jobs}
#emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the library
#normal just testing
data_stan = emiles.data_stan()
dataforstan = data_stan["1"]
modelfile = data_stan["2"]
fitfile = data_stan["3"]
#emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,"0")
N=20
errors=[0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
N=len(errors)
meanerry=np.zeros(N)
meanage=np.zeros(N)
sderry=np.zeros(N)
sdage=np.zeros(N)
agerhat=np.zeros(N)
erryrhat=np.zeros(N)
AGE_n=np.zeros(N)
s_n=np.zeros(N)
plt.show()
yerror= np.zeros([2,N])
for i in range(N):
    print(emiles.lages[i])
    parameters = {'ageyr':AGE, 'mfeh': 0,'a': 0, 'b': 0, 't0yr': 0,'tauyr':0,"error":errors[i]}
    emiles.create_model("a.ref",parameters)
    
    emiles.load_observation("a.ref")
    data_stan = emiles.data_stan()
    dataforstan = data_stan["1"]
    modelfile = data_stan["2"]
    fitfile = data_stan["3"]
    T=True
    count=0
    while T:
        count+=1
        emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,parameters,'R')
        stanFit=emiles.resolve_parameters
        nlines = 11 + emiles.nparameters
        output = str(stanFit).split('\n')
        for item in output[1:nlines]:
            print(item)
        irhat = stanFit.summary()['summary_colnames']
        irhat = irhat.index("Rhat")
        irhat = stanFit.summary()["summary"][:, irhat]
        if (irhat[0] < 1.1 and irhat[1]) or count > 10:
            T=False
        else:
            T = True
    print(i/N,"%")
    xages = np.zeros(len(stanFit['age']))
    for j in range(len(stanFit['age'])):
        xf = stanFit['age'][j]
        bf = int(xf) - 1
        xages[j] = emiles.lages[bf] + (xf - 1. - bf) * (emiles.lages[bf + 1] - emiles.lages[bf])
    meanage[i] = np.mean(xages)
    sdage[i] = np.std(xages, ddof=1)
    agerhat[i]=irhat[0]
    hdiage = HDIofMCMC(xages, credMass=0.95)
    xerry = np.zeros(len(stanFit['erry']))
    for j in range(len(stanFit['erry'])):
        xf = stanFit['erry'][j]
        bf = int(xf) - 1
        xages[j] = emiles.lages[bf] + (xf - 1. - bf) * (emiles.lages[bf + 1] - emiles.lages[bf])
    meanerry[i] = np.mean(stanFit['erry'])
    sderry[i] = np.std(stanFit['erry'], ddof=1)
    erryrhat[i]=irhat[1]
    yerror[0,i]=meanage[i]-hdiage[0]
    yerror[1,i]=hdiage[1]-meanage[i]
    parameters = {'ageyr': AGE, 'mfeh': 0,'a': 0, 'b': 0, 't0yr': 0,'tauyr':0,"error":0}
    spobs=emiles.spobs
    emiles.create_model("a.ref",parameters)    
    emiles.load_observation("a.ref")
    s_n[i]=sum(emiles.spobs/abs(emiles.spobs-spobs))/emiles.infilt
    AGE_n[i]=AGE
    




    
plt.figure()
plt.errorbar(s_n,meanage,yerr=yerror,fmt='o',label="fit vs mock")
plt.plot(s_n,AGE_n)
plt.ylabel("error_in_age")
plt.xlabel("S/N")
#plt.plt(emiles.lages[i],emiles.lages[i])
plt.show()
f = open("age_emilesfilterse_01.txt",'w')
f.write("meanerry sderry erryrhat meanage sdage agerhat real_ages \n")

for i in range(N):
    f.writelines(['%.5f' % meanerry[i]," ",'%.5f' % sderry[i]," ",'%.5f' % erryrhat[i]," ",'%.5f' % meanage[i]," ",'%.5f' % sdage[i]," ",'%.5f' % agerhat[i],'%.5f' % emiles.lages[i],"\n"])

