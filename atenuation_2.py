from __future__ import division 
from __future__ import print_function
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.stats import norm, beta
import pickle
from pystan import StanModel
from astropy.io import fits
from fjg import *
import mcmcsp_fun as mcsp
import pyphot
from graphics import spectrums

itypemod = "4"
itypef = "8"
cerrob = "N"
bin = 100
emiles = spectrums()
emiles.itypef = itypef
emiles.itypemod = itypemod
emiles.cerrob = cerrob
emiles.Rv=[2.0,2.5,3.,3.5,4.0,4.5,5.0]
emiles.load_spectra(bin)
ageyr=0
mfeh=-0.9
a=5.0
b=20.0
t0yr=0
tauyr=0
error=0.02
Rv=3.1
parameters = {'ageyr': ageyr, 'mfeh': mfeh,'a': a, 'b': b, 't0yr': t0yr,'tauyr':tauyr,"error":error,"Rv":Rv}
emiles.create_model("w.ref",parameters)
emiles.load_observation("w.ref")
print(emiles.spobs)
chains = 4 # 3
warmup = 1500  # 500
niter = 9500  # 9500
thin = 1
n_jobs = 4
stanparameters = {'chains': chains, 'warmup': warmup,'niter': niter, 'thin': thin, 'n_jobs': n_jobs}

#normal just testing
data_stan = emiles.data_stan()
dataforstan = data_stan["1"]
modelfile = data_stan["2"]
fitfile = data_stan["3"]
emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,"0")


#for challenger


#emiles.atenuation(emiles.Rv)
emiles.redshift(0.21)
emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the librari

f=open("Challenge-master/total_file_z021.dat",'r')
num_lines = sum(1 for line in f)
f.close()
f=open("Challenge-master/total_file_z021.dat",'r')


N=int(num_lines/2)
meanfeh=np.zeros(N)
meanage=np.zeros(N)
sdfeh=np.zeros(N)
sdage=np.zeros(N)
hdifeh=np.zeros(N)
hdiage=np.zeros(N)
agerhat=np.zeros(N)
fehrhat=np.zeros(N)
meanmus=np.zeros(N)
meantheta=np.zeros(N)
musrhat=np.zeros(N)
thetarhat=np.zeros(N)
xa=np.zeros(N)
xb=np.zeros(N)
Rv=np.zeros(N)
RvRhat=np.zeros(N)

#compile
data_stan = emiles.data_stan()
dataforstan = data_stan["1"]
modelfile = data_stan["2"]
fitfile = data_stan["3"]

emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,'C')
for i in range(1):#N):
    emiles.spobs=np.fromstring(f.readline(),dtype=float,sep=' ')
    emiles.spobse=np.fromstring(f.readline(),dtype=float,sep=' ')
    summodel = sum(emiles.spobs)
    emiles.spobs= emiles.spobs / summodel * emiles.infilt
    emiles.spobse= emiles.spobse / summodel * emiles.infilt
    data_stan = emiles.data_stan()
    dataforstan = data_stan["1"]
    modelfile = data_stan["2"]
    fitfile = data_stan["3"]
    #emiles.run_stan2(dataforstan, modelfile, fitfile, stanparameters,'R')
    T=True
    count=0
    while T:
        count+=1
        emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,'0')
        itypef = "8"
        itypemod ="4"
        cerrob = "N"
        bin =10
        emiles2 = spectrums()
        emiles2.itypef = itypef
        emiles2.itypemod = itypemod
        emiles2.cerrob = cerrob
        emiles2.load_spectra(bin)
        emiles2.create_model("a.ref")
        emiles2.load_observation("a.ref")
        #normal just testing
        iRv2 = 1 
        while (iRv2 < Rv):
            iRv2 = iRv2 + 1 
        weigRv1 = iRv2 - Rv 
        weigRv2 = 1. - weigRv1

        j1 =  (iRv2-2)*nfehs*nages  ;
        j2 =  (iRv2-1)*nfehs*nages  ;
        
        emiles2.Rv=[2.0,2.5,3.,3.5,4.0,4.5,5.0]
        emiles.redshift(0.21)
        emiles2.atenuation(emiles.Rv)
        
        
        #emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the libraryp
        
        #for k in range(emiles.nages*emiles.nfehs*len(emiles.Rv)):
        #    summodels = sum(emiles.modelsb[k, ])
        #    emiles.modelsb[k, ] = emiles.modelsb[k, ] / summodels * emiles.infilt
        F = open(fitfile, 'rb')
        stanFit = pickle.load(F)
        F.close()
        output = str(stanFit).split('\n')
        nlines = 11 + emiles.nparameters + 2
        for item in output[1:nlines]:
            print(item)
        irhat = stanFit.summary()['summary_colnames']
        irhat = irhat.index("Rhat")
        irhat = stanFit.summary()["summary"][:, irhat]
        if (irhat[0] and irhat[1] < 1.1) or count > 10:
            T=False
        else:
            T = True
            


    print(i/N,"%")
    xfehs = np.zeros(len(stanFit['feh']))
    for j in range(len(stanFit['feh'])):
       xf = stanFit['feh'][j]
       bf = int(xf) - 1
       xfehs[j] = emiles.lfehs[bf] + (xf - 1. - bf) * (emiles.lfehs[bf + 1] - emiles.lfehs[bf])
    meanfeh[i] = np.mean(xfehs)
    sdfeh[i] = np.std(xfehs, ddof=1)
    fehrhat[i]=irhat[2]
    
    #hdifeh[i] = HDIofMCMC(xfehs, credMass=0.95)
    if emiles.itypef == "3" or emiles.itypef == "4" or emiles.itypef == "8":
        xxages = stanFit['mu'] * (emiles.x1 - emiles.x0) + emiles.x0
        meanage[i] = np.mean(xxages)
        sdage[i] = np.std(xxages, ddof=1)
        hdiage = HDIofMCMC(xxages, credMass=0.95)
        xmus = (stanFit['mu'])
        meanmus[i] = np.mean(xmus)
        musrhat[i] = irhat[0]
        xtheta = (stanFit['theta'])
        meantheta[i] = np.mean(xtheta)
        thetarhat[i] = irhat[1]
        xa[i] = np.mean(stanFit['a'])
        xb[i] = np.mean(stanFit['b'])
        if emiles.itypef== "8":
            Rv[i]=np.mean(stanFit['Rv'])
            RvRhat[i]=irhat[3]
        
    elif emiles.itypef ==  "1" or emiles.itypef == "2":
        if emiles.itypemod == "1" or emiles.itypemod == "3":
            xages = (stanFit['age'] - 1.) * emiles.stepage + emiles.minage
        elif emiles.itypemod == "2":
            print("not implemented yet ")
        elif emiles.itypemod == "4" or emiles.itypemod == "5":
            xages = np.zeros(len(stanFit['age']))
            for j in range(len(stanFit['age'])):
                xf = stanFit['age'][j]
                bf = int(xf) - 1
                xages[j] = emiles.lages[bf] + (xf - 1. - bf) * (emiles.lages[bf + 1] - emiles.lages[bf])
        meanage[i] = np.mean(xages)
        sdage[i] = np.std(xages, ddof=1)
        agerhat[i]=irhat[0]
        #hdiage[i] = HDIofMCMC(xages, credMass=0.95)
                        
f = open("pruebaatenuationwith_error_bars2.txt",'w')
if emiles.itypef=="2" :
    f.write("meanfeh sdfeh fehrhat meanage sdage agerhat \n")
    for i in range(N):
        f.writelines(['%.5f' % meanfeh[i]," ",'%.5f' % sdfeh[i]," ",'%.5f' % fehrhat[i]," ",'%.5f' % meanage[i]," ",'%.5f' % sdage[i]," ",'%.5f' % agerhat[i],"\n"])
    f.close()
elif emiles.itypef=="8":
    f.write("meanfeh sdfeh fehrhat meanage sdage meanmus musrhat meantheta thetarhat meanxa meanxb Rv RvRhat \n")
    for i in range(N):
        f.writelines(['%.5f' % meanfeh[i]," ",'%.5f' % sdfeh[i]," ",'%.5f' % fehrhat[i]," ",'%.5f' % meanage[i]," ",'%.5f' % sdage[i]," ",'%.5f' % meanmus[i]," ",'%.5f' % musrhat[i]," ",'%.5f' % meantheta[i] ," ",'%.5f' % thetarhat[i]," ",'%.5f' % xa[i]," ",'%.5f' % xb[i]," ",'%.5f' % Rv[i]," ",'%.5f' % RvRhat[i], "\n" ] )
    f.close()
    

# 0.00. plt.plot(emiles.slambdab, emiles.modelsb[1, ])
# emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the library
# metalicidad=np.zeros((emiles.nfehs,emiles.infilt))
# for i in range(emiles.nfehs):
#    metalicidad[i,]=emiles.modelsb[i*emiles.nages+emiles.nages-10,]
# plt.loglog(emiles.lages,emiles.modelsb[8*emiles.nages:9*emiles.nages, ])
# plt.plot(emiles.lfehs,metalicidad)
# plt.show()
# emiles.save_spectrum_as_fits("Phot_JPAS_Emiles/")
# emiles.run_stan2(dataforstan, modelfile, fitfile, stanparameters,"R")
