data {
  int<lower=0> npix ;
  int<lower=0> nages ;
  int<lower=0> nfehs ;
  vector[npix] yobs ;
  matrix[nages*nfehs,npix] ymod ;
}
parameters {
  real<lower=1.,upper=nages> age ;
  real<lower=1.,upper=nfehs> feh ;
  real<lower=0.> erry ;
}
model{
  vector[npix] yinterp ;
  vector[npix] yinterp1 ;
  vector[npix] yinterp2 ;
  real weiga1 ;
  real weiga2 ;
  real weigz1 ;
  real weigz2 ;
  int ia2 ;
  int iz2 ;
  int j1 ;
  int j2 ;
  int k1 ;
  int k2 ;

  erry ~ normal(0.,1.) ;

  ia2 = 1 ;
  while (ia2 < age)
    ia2 = ia2 + 1 ;
  weiga1 = ia2 - age ;
  weiga2 = 1. - weiga1 ;
  
  iz2 = 1 ;
  while (iz2 < feh)
    iz2 = iz2 + 1 ;
  weigz1 = iz2 - feh ;
  weigz2 = 1. - weigz1 ;
  
  j1 = (iz2 - 2) * nages + ia2 - 1 ;
  j2 = j1 + 1 ;
  yinterp1 = (ymod[j1]*weiga1 + ymod[j2]*weiga2)' ;
  k1 = (iz2 - 1) * nages + ia2 - 1 ;
  k2 = k1 + 1 ;
  yinterp2 = (ymod[k1]*weiga1 + ymod[k2]*weiga2)' ;
  yinterp = yinterp1*weigz1 + yinterp2*weigz2;
  
  yobs ~ normal(yinterp, erry) ;
}
