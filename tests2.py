from __future__ import division 
from __future__ import print_function
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.stats import norm, beta
import pickle
from pystan import StanModel
from astropy.io import fits
from fjg import *
import mcmcsp_fun as mcsp
import pyphot
from graphics import spectrums

itypemod = "5"
itypef = "2"
cerrob = "N"
bin = 100

print('Type of models:')
if itypemod =="1":
    print('    1 = MILES')
elif itypemod == "2":
    print('    2 = Bruzual (9 bands)')
elif itypemod == "3":
    print('    3 = Bulges (Gorgas & Jablonka) spectra')
elif itypemod == "4":
    print('    4 = EMILES')
elif itypemod == "5":
    print('    5=JPAS_EMILES')
#itypemod = raw_input('? ')
print('Type of fit:')
if itypef == "1":
    print('    1 = single burst (only age, solar Z)')
if itypef == "2":
    print('    2 = single burst (age and metallicity)')
if itypef == "3":
    print('    3 = beta SFH (solar Z)')
if itypef == "4":
    print('    4 = beta SFH (variable Z)')
if itypef == "5":
    print('    5 = delayed exponential SFH (solar Z)')
if itypef == "6":
    print('    6 = delayed exponential SFH (variable Z)')
# itypef = raw_input('? ')
# bin = int(raw_input('Bin? (0=none) '))
# cerrorb = raw_input('Individual error bars (Y/N)? ')
print("Individual error bars",cerrob)
print("Bin =",bin)
emiles = spectrums()
emiles.itypef = itypef
emiles.itypemod = itypemod
emiles.cerrob = cerrob
emiles.load_spectra(bin)
ageyr=10.5
mfeh=0
a=0
b=0
t0yr=0
tauyr=0
error=0.02
parameters = {'ageyr': ageyr, 'mfeh': mfeh,'a': a, 'b': b, 't0yr': t0yr,'tauyr':tauyr,"error":error}
emiles.create_model("a.ref",parameters)
emiles.load_observation("a.ref")
chains = 10 # 3
warmup =3000  # 500
niter = 9500  # 9500
thin = 1
n_jobs =10
stanparameters = {'chains': chains, 'warmup': warmup,'niter': niter, 'thin': thin, 'n_jobs': n_jobs}
#emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the library
#normal just testing
data_stan = emiles.data_stan()
dataforstan = data_stan["1"] 
modelfile = data_stan["2"]
fitfile = data_stan["3"]
#emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,"0")
lfehs=[-2.1,-2.0,-1.9,-1.8,-1.7,-1.6,-1.5,-1.4,-1.3,-1.2,-1.1,-1.0,-0.9,-0.8,-0.7,-0.6,-0.5,-0.4,-0.3,-0.2,-0.1,0.0,0.1,0.2]
N=len(lfehs)
meanerry=np.zeros(N)
meanage=np.zeros(N)
meanfeh=np.zeros(N)
sderry=np.zeros(N)
sdage=np.zeros(N)
agerhat=np.zeros(N)
erryrhat=np.zeros(N)
plt.show()
yerror= np.zeros([2,N])
for i in range(N):
    print(emiles.lages[i])
    parameters = {'ageyr': 1.5, 'mfeh': lfehs[i],'a': 0, 'b': 0, 't0yr': 0,'tauyr':0,"error":0.02}
    emiles.create_model("a.ref",parameters)
    emiles.load_observation("a.ref")
    data_stan = emiles.data_stan()
    dataforstan = data_stan["1"]
    modelfile = data_stan["2"]
    fitfile = data_stan["3"]
    T=True
    count=0
    while T:
        count+=1
        emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,'R')
        stanFit=emiles.resolve_parameters
        nlines = 11 + emiles.nparameters
        output = str(stanFit).split('\n')
        for item in output[1:nlines]:
            print(item)
        irhat = stanFit.summary()['summary_colnames']
        irhat = irhat.index("Rhat")
        irhat = stanFit.summary()["summary"][:, irhat]
        if (irhat[0] < 1.1 and irhat[1]<1.1) or count > 10:
            T=False
        else:
            T = True
    print(i/N,"%")
    xfehs = np.zeros(len(stanFit['feh']))
    for j in range(len(stanFit['feh'])):
        xf = stanFit['feh'][j]
        bf = int(xf) - 1
        xfehs[j] = emiles.lfehs[bf] + (xf - 1. - bf) * (emiles.lfehs[bf + 1] - emiles.lfehs[bf])
    meanfeh[i] = np.mean(xfehs)
    sdfeh = np.std(xfehs, ddof=1)
    hdifeh = HDIofMCMC(xfehs, credMass=0.95)
    
    yerror[0,i]=meanfeh[i]-hdifeh[0]
    yerror[1,i]=hdifeh[1]-meanfeh[i]
plt.figure()
plt.errorbar(lfehs,meanfeh,yerr=yerror,fmt='.',label="fit vs mock")
plt.plot(lfehs,lfehs,label="mock vs mock")
plt.legend()
plt.xlabel("mock_feh")
plt.ylabel("fit_feh")
#plt.plt(emiles.lages[i],emiles.lages[i])
plt.show()
#f = open("age_emilesfilterse_01.txt",'w')
#f.write("meanerry sderry erryrhat meanage sdage agerhat real_ages \n")

#for i in range(N):
 #   f.writelines(['%.5f' % meanerry[i]," ",'%.5f' % sderry[i]," ",'%.5f' % erryrhat[i]," ",'%.5f' % meanage[i]," ",'%.5f' % sdage[i]," ",'%.5f' % agerhat[i],'%.5f' % emiles.lages[i],"\n"])
#f.close()
    
    #     xfehs = np.zeros(len(stanFit['feh']))
#     for j in range(len(stanFit['feh'])):
#        xf = stanFit['feh'][j]
#        bf = int(xf) - 1
#        xfehs[j] = emiles.lfehs[bf] + (xf - 1. - bf) * (emiles.lfehs[bf + 1] - emiles.lfehs[bf])
#     meanfeh[i] = np.mean(xfehs)
#     sdfeh[i] = np.std(xfehs, ddof=1)
#     fehrhat[i]=irhat[1]
#     #hdifeh[i] = HDIofMCMC(xfehs, credMass=0.95)
#     if emiles.itypemod == "1" or emiles.itypemod == "3":
#         xages = (stanFit['age'] - 1.) * emiles.stepage + emiles.minage
#     elif emiles.itypemod == "2":
#         print("not implemented yet ")
#     elif emiles.itypemod == "5" or emiles.itypemod == "5":
#         xages = np.zeros(len(stanFit['age']))
#         for j in range(len(stanFit['age'])):
#             xf = stanFit['age'][j]
#             bf = int(xf) - 1
#             xages[j] = emiles.lages[bf] + (xf - 1. - bf) * (emiles.lages[bf + 1] - emiles.lages[bf])
#     meanage[i] = np.mean(xages)
#     sdage[i] = np.std(xages, ddof=1)
#     agerhat[i]=irhat[0]
#     #hdiage[i] = HDIofMCMC(xages, credMass=0.95)




#print(emiles.resolve_fv)
#print(emiles.resolve_parameters)

#for challenger
# emiles.redshift(0.5)
# 


# f=open("Challenge-master/total_file_z050.dat",'r')
# num_lines = sum(1 for line in f)
# f.close()
# f=open("Challenge-master/total_file_z050.dat",'r')


# N=int(num_lines/2)
# meanfeh=np.zeros(N)
# meanage=np.zeros(N)
# sdfeh=np.zeros(N)
# sdage=np.zeros(N)
# hdifeh=np.zeros(N)
# hdiage=np.zeros(N)
# agerhat=np.zeros(N)
# fehrhat=np.zeros(N)
# #compile
# data_stan = emiles.data_stan()
# dataforstan = data_stan["1"]
# modelfile = data_stan["2"]
# fitfile = data_stan["3"]

# emiles.run_stan2(dataforstan, modelfile, fitfile, stanparameters,'C')
# for i in range(N):
#     emiles.spobs=np.fromstring(f.readline(),dtype=float,sep=' ')
#     emiles.spobse=np.fromstring(f.readline(),dtype=float,sep=' ')
#     summodel = sum(emiles.spobs)
#     emiles.spobs= emiles.spobs / summodel * emiles.infilt
#     emiles.spobse= emiles.spobse / summodel * emiles.infilt
#     data_stan = emiles.data_stan()
#     dataforstan = data_stan["1"]
#     modelfile = data_stan["2"]
#     fitfile = data_stan["3"]
#     #emiles.run_stan2(dataforstan, modelfile, fitfile, stanparameters,'R')
#     T=True
#     count=0
#     while T:
#         count+=1
#         emiles.run_stan2(dataforstan, modelfile, fitfile, stanparameters,'R')
#         F = open(fitfile, 'rb')
#         stanFit = pickle.load(F)
#         F.close()
#         nlines = 11 + emiles.nparameters
#         output = str(stanFit).split('\n')
#         for item in output[1:nlines]:
#             print(item)
#         irhat = stanFit.summary()['summary_colnames']
#         irhat = irhat.index("Rhat")
#         irhat = stanFit.summary()["summary"][:, irhat]
#         if (irhat[0] and irhat[1] < 1.1) or count > 10:
#             T=False
#         else:
#             T = True
            
#     print(i/N,"%")
#     xfehs = np.zeros(len(stanFit['feh']))
#     for j in range(len(stanFit['feh'])):
#        xf = stanFit['feh'][j]
#        bf = int(xf) - 1
#        xfehs[j] = emiles.lfehs[bf] + (xf - 1. - bf) * (emiles.lfehs[bf + 1] - emiles.lfehs[bf])
#     meanfeh[i] = np.mean(xfehs)
#     sdfeh[i] = np.std(xfehs, ddof=1)
#     fehrhat[i]=irhat[1]
#     #hdifeh[i] = HDIofMCMC(xfehs, credMass=0.95)
#     if emiles.itypemod == "1" or emiles.itypemod == "3":
#         xages = (stanFit['age'] - 1.) * emiles.stepage + emiles.minage
#     elif emiles.itypemod == "2":
#         print("not implemented yet ")
#     elif emiles.itypemod == "5" or emiles.itypemod == "5":
#         xages = np.zeros(len(stanFit['age']))
#         for j in range(len(stanFit['age'])):
#             xf = stanFit['age'][j]
#             bf = int(xf) - 1
#             xages[j] = emiles.lages[bf] + (xf - 1. - bf) * (emiles.lages[bf + 1] - emiles.lages[bf])
#     meanage[i] = np.mean(xages)
#     sdage[i] = np.std(xages, ddof=1)
#     agerhat[i]=irhat[0]
#     #hdiage[i] = HDIofMCMC(xages, credMass=0.95)
                        
# f = open("prueba050with_error_bars.txt",'w')
# f.write("meanfeh sdfeh fehrhat meanage sdage agerhat \n")
# for i in range(N):
#     f.writelines(['%.5f' % meanfeh[i]," ",'%.5f' % sdfeh[i]," ",'%.5f' % fehrhat[i]," ",'%.5f' % meanage[i]," ",'%.5f' % sdage[i]," ",'%.5f' % agerhat[i],"\n"])
# f.close()

    

# 0.00. plt.plot(emiles.slambdab, emiles.modelsb[1, ])
# emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the library
# metalicidad=np.zeros((emiles.nfehs,emiles.infilt))
# for i in range(emiles.nfehs):
#    metalicidad[i,]=emiles.modelsb[i*emiles.nages+emiles.nages-10,]
# plt.loglog(emiles.lages,emiles.modelsb[8*emiles.nages:9*emiles.nages, ])
# plt.plot(emiles.lfehs,metalicidad)
# plt.show()
# emiles.save_spectrum_as_fits("Phot_JPAS_Emiles/")
# emiles.run_stan2(dataforstan, modelfile, fitfile, stanparameters,"R")
