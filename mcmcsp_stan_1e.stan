data {
  int<lower=0> npix ;
  int<lower=0> nages ;
  vector[npix] yobs ;
  matrix[nages,npix] ymod ;
}
parameters {
  real<lower=1.,upper=nages> age ;
  real<lower=0.> erry ;
}
model{
  vector[npix] yinterp ;
  real weig1 ;
  real weig2 ;
  int i1 ;
  int i2 ;

  erry ~ normal(0.,1.0) ;
  i2 = 1 ;
  while (i2 < age)
    i2 = i2 + 1 ;
  i1 = i2 - 1 ;
  weig1 = i2 - age ;
  weig2 = 1. - weig1 ;
  yinterp = (ymod[i1]*weig1 + ymod[i2]*weig2)' ;
  yobs ~ normal(yinterp, erry) ;
}
