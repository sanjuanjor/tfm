import matplotlib.pyplot as plt
import numpy as np


def extintion(x, Rv):
    if x <= 1.1:
        return 0.574 * x**(1.61) - (1. / Rv) * 0.527 * x**(1.61)
    elif x <= 3.3:
        y = (x - 1.82)
        return 1.0 + 0.17699 * y - 0.50447 * y**2 - 0.02427 * y**3 + 0.72085 * y**4 + 0.01979 * y**5 - 0.77530 * y**6 + 0.32999 * y**7 + (1. / Rv) * (1.41338 * y + 2.28305 * y**2 + 1.07233 * y**3 - 5.38434 * y**4 - 0.62251 * y**5 + 5.30260 * y**6 - 2.09002 * y**7)
    elif x <= 5.9:
        return 1.752 - 0.316 * x - 0.104 / ((x - 4.67)**2 + 0.341) + (1. / Rv) * (-3.090 + 1.825 * x + 1.206 / ((x - 4.62)**2 + 0.263))
    elif x <= 8.0:
        return 1.752 - 0.316 * x - 0.104 / ((x - 4.67)**2 + 0.341) - 0.04473 * (x - 5.9)**2 - 0.009779 * (x - 5.9)**3 + (1. / Rv) * (-3.090 + 1.825 * x + 1.206 / ((x - 4.62)**2 + 0.263) + 0.2130 * (x - 5.9)**2 + 0.1207 * (x - 5.9)**3)
    elif x <= 10:
        return -1.073 - 0.628 * (x - 8) + 0.137 * (x - 8)**2 - 0.070 * (x - 8)**3 + (1. / Rv) * (13.670 + 4.257 * (x - 8) - 0.420 * (x - 8)**2 + 0.347 * (x - 8)**3)


#x = np.linspace(0.3, 10, 500)
#extintion_curve1 = np.zeros(500)
# extintion_curve2 = np.zeros(500)
# extintion_curve3 = np.zeros(500)

# for j in range(500):
#     extintion_curve1[j] = extintion(x[j], 2)
#     extintion_curve2[j] = extintion(x[j], 3.1)
#     extintion_curve3[j] = extintion(x[j], 5)
# plt.plot(x, extintion_curve1)
# plt.plot(x, extintion_curve2)
# plt.plot(x, extintion_curve3)
# plt.show()
