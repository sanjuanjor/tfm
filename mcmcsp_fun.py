# mcmcsp_fun
from __future__ import division
from __future__ import print_function
import numpy as np
from scipy.stats import norm, beta

# Funciones para crear espectros predichos

def predmodel12(tip, infilt, age, met, nages, nfehs, modelsb) :
    """
    tip = 1 (only age), 2 (age & metal)
    """    
    ypred = np.zeros(infilt) 
    modelinter = np.zeros((nfehs,infilt))

    i1 = int(np.floor(age))
    i2 = i1 +1
    weight1 = i2 - age
    weight2 = 1. - weight1

    for m in range(nfehs) : 
        for j in range(infilt):
            i1m = i1 + nages * m
            i2m = i1m + 1
            modelinter[m,j] = modelsb[i1m,j]*weight1 + modelsb[i2m,j]*weight2

    if tip == "1" :
        ypred = modelinter[0,]
    else :
        i1 = int(np.floor(met))
        i2 = i1 +1
        weight1 = i2 - met
        weight2 = 1. - weight1
        for j in range(infilt):
            ypred[j] = modelinter[i1,j]*weight1 + modelinter[i2,j]*weight2

    return ypred
    
def predmodel34(tip, infilt, a, b, met, nages, nfehs, modelsb, xages, incage) :
    """
    tip = 1 (only age), 2 (age & metal)
    """  
    ypred = np.zeros(infilt) 
    modelinter = np.zeros((nfehs,infilt))
    
    sfh = beta.pdf(xages, a=a, b=b) 
                
    for m in range(nfehs) : 
        for i in range(nages):
            for j in range(infilt):
                im = i + nages*m
                modelinter[m,j] = modelinter[m,j] + modelsb[im,j]*sfh[i]*incage[i]
                
    if tip == "3" :
        ypred = modelinter[0,]
    else :
        i1 = int(np.floor(met))
        i2 = i1 +1
        weight1 = i2 - met
        weight2 = 1. - weight1
        for j in range(infilt):
            ypred[j] = modelinter[i1,j]*weight1 + modelinter[i2,j]*weight2      
         
    summodel = sum(ypred[:])
    ypred = ypred/summodel*infilt
        
    return ypred
    
def predmodel56(tip, infilt, t, tau, met, nages, nfehs, modelsb, limage) :
    """
    tip = 1 (only age), 2 (age & metal)
    """  
    ypred = np.zeros(infilt) 
    modelinter = np.zeros((nfehs,infilt))
    wei = np.zeros(nages) 
    
    for i in range(nages) :
        print(i)
        ta = max(0., t-limage[i+1])
        tb = max(0., t-limage[i])
        wei[i] = tau*((ta+tau)*np.exp(-ta/tau) - (tb+tau)*np.exp(-tb/tau))   

    for m in range(nfehs) : 
        for i in range(nages):
            for j in range(infilt):
                im = i + nages*m
                modelinter[m,j] = modelinter[m,j] + modelsb[im,j]*wei[i]
                
    if tip == "5" :
        ypred = modelinter[0,]
    else :
        i1 = int(np.floor(met))
        i2 = i1 +1
        weight1 = i2 - met
        weight2 = 1. - weight1
        for j in range(infilt):
            ypred[j] = modelinter[i1,j]*weight1 + modelinter[i2,j]*weight2      
         
    summodel = sum(ypred[:])
    ypred = ypred/summodel*infilt
        
    return ypred
                  
