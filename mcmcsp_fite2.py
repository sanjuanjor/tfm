# -*- coding: utf-8 -*-
# mcmcsp_fite.pyR
# @ Javier Gorgas

from __future__ import division
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.stats import norm, beta
import pickle
 
from pystan import StanModel
from astropy.io import fits

from fjg import *
import mcmcsp_fun as mcsp

print('Type of models:')
print('    1 = MILES')
print('    2 = Bruzual (9 bands)')
print('    3 = Bulges (Gorgas & Jablonka) spectra')
itypemod = raw_input('? ')

if itypemod == "1" or itypemod == "3" :
    # rango de edades (en log(years) de los modelos de MILES
    minage = 7.80
    maxage = 10.26
    stepage = 0.05
    lages = np.arange(minage, maxage, stepage)
    nages = lages.size

    # rango de metalicidades
    lfehs = [-2.32, -1.71, -1.31, -0.71, -0.40, 0, 0.22]
    nfehs = len(lfehs)

    if itypemod == "1" : 
        npixels = 4300
    elif itypemod == "3" :
        npixels = 1024

elif itypemod == "2" :
    # rango de edades (en log(years) de los modelos de Bruzual (9 bands)
    minage = 6.0
    maxage = 10.01
    stepage = 0.1
    lages = np.arange(minage, maxage, stepage)
    nages = lages.size

    # rango de metalicidades
    lfehs = [-0.699, -0.397, 0., 0.397]
    nfehs = len(lfehs)

    npixels = 9

print('Type of fit:')
print('    1 = single burst (only age, solar Z)')
print('    2 = single burst (age and metallicity)')
print('    3 = beta SFH (solar Z)')
print('    4 = beta SFH (variable Z)')
print('    5 = delayed exponential SFH (solar Z)')
print('    6 = delayed exponential SFH (variable Z)')
itypef = raw_input('? ')
#itypef = "2"
cerrorb = "N"
cerrorb = raw_input('Individual error bars (Y/N)? ')

if itypef == "1" :
    # numero de parametros que ajustara el modelo
    nparameters = 1
    models = np.zeros((nages,npixels))
    nfehs = 1
    lfehs = [0]
    
elif itypef == "2" :
    nparameters = 2
    models = np.zeros((nages*nfehs,npixels))
    
elif itypef == "3" :
    nparameters = 2
    models = np.zeros((nages,npixels))
    nfehs = 1
    lfehs = [0]

elif itypef == "4" :
    nparameters = 3
    models = np.zeros((nages*nfehs,npixels))

elif itypef == "5" :
    nparameters = 2
    models = np.zeros((nages,npixels))
    nfehs = 1
    lfehs = [0]

elif itypef == "6" :
    nparameters = 3
    models = np.zeros((nages*nfehs,npixels))
else :
    print('ERROR: Not implemented')
        
# lectura de los modelos originales

if itypemod == "1" or itypemod == "3" :

    dire = "MILES_SSPs/MILES_Padova00_un_1.30_fits/"

    k = 0
    for m in range(nfehs) : 
        chmet = '%+5.2f' % lfehs[m]
        chmet = 'm' + chmet[1:] if lfehs[m] < 0 else 'p' + chmet[1:]
        print(m,chmet)
        for i in range(nages):
            chage = '%07.4f' % (10**lages[i]*1E-9)
            filename = dire + "Mun1.30Z" + chmet + "T" + chage + "_iPp0.00_baseFe.fits"
            if itypemod == "3" :
                filename = filename + ".usbr.fits"
            hdul = fits.open(filename)
            models[k,] = hdul[0].data
            hdul.close()
            if itypef == "1" or itypef == "2" :
                # se normalizan
                summodels = sum(models[k,])
                models[k,] = models[k,]/summodels*npixels
            k += 1
      
    # Hacemos un binning (o no) (solo para MILES)
    bin = 10
    bin = int(raw_input('Bin? (0=none) '))
    if bin > 1 : 
        infilt  = int(npixels/bin)
        modelsb = np.zeros((nages*nfehs,infilt))
        for i in range(nages*nfehs):
            for j in range(infilt):
                j1 = j*bin
                j2 = j1 + bin
                modelsb[i,j] = np.mean(models[i,j1:j2])
    else:
        infilt  = npixels
        modelsb = models

elif itypemod == "2" :
    infilt  = npixels
    modelsb = np.zeros((nages*nfehs,infilt))   
    filebruzual = "model.BC03.bc2003.Padova1994.chab.ssp.z_2.23.flux" 
    fileb = open(filebruzual,'r')   
    fileb.readline()
    if nfehs == 1 :
        for i in range(nages*2) : fileb.readline()
    k = 0
    for m in range(nfehs) :  
        for i in range(nages):  
            st = fileb.readline().split()
            for j in range(npixels):
                modelsb[k,j] = float(st[j+3])
            if itypef == "1" or itypef == "2" :
                # se normalizan
                summodels = sum(modelsb[k,])
                modelsb[k,] = modelsb[k,]/summodels*npixels
            k += 1
            
    fileb.close()
                        
# Galaxy

mockfile = raw_input('Galaxy file name? ')

if itypemod == "1" :
    F = open(mockfile,'rb')
    spobs = pickle.load(F)
    F.close()
    print(len(spobs),infilt)
    if len(spobs) != infilt :
        print('ERROR: mock galaxy has a wrong binning')

elif itypemod == "2" :
    spobs = np.zeros(npixels)
    spobse = np.zeros(npixels)
    ldo = np.zeros(npixels)
    # de momento solo lee la primera línea 
    mockfile = "esf_13162.reg1"
    fileg = open(mockfile,'r')  
    st = fileg.readline().split() 
    for j in range(npixels):
        spobs[j] = float(st[(j+1)*5])
        spobse[j] = float(st[(j+1)*5+1])
        ldo[j] = float(st[(j+1)*5-1])*1E-3 # en micras
    # Cuidado: en el fichero de datos el 3 y el 4 están intercambiados:
    xtemp = spobs[4]
    spobs[4] = spobs[3]
    spobs[3] = xtemp
    xtemp = spobse[4]
    spobse[4] = spobse[3]
    spobse[3] = xtemp
    xtemp = ldo[4]
    ldo[4] = ldo[3]
    ldo[3] = xtemp
    #
    summodel = sum(spobs)
    spobs = spobs/summodel*infilt
    spobse = spobse/summodel*infilt

if itypemod == "3" :
    spobs = np.zeros(npixels)
    hdul = fits.open(mockfile)
    spobs[:] = hdul[0].data
    hdul.close()
    summodel = sum(spobs)
    spobs = spobs/summodel*infilt
    
    print(len(spobs),infilt)
    if len(spobs) != infilt :
        print('ERROR: mock galaxy has a wrong binning')   
         
# Datos para Stan:
if itypef == "1" :
    if cerrorb == "N" :
        dataforstan = {'npix': infilt, 'nages': nages, 'yobs': spobs, 
            'ymod': modelsb}
    elif cerrorb == "Y" : 
        dataforstan = {'npix': infilt, 'nages': nages, 'yobs': spobs, 
            'ymod': modelsb, 'yerr': spobse}
    modelfile = 'ultimo_modelo_21.pkl'
    fitfile = 'ultimo_ajuste_21.pkl'
    
elif itypef == "2":
    if cerrorb == "N" :
        dataforstan = {'npix': infilt, 'nages': nages, 'nfehs': nfehs, 'yobs': spobs, 
            'ymod': modelsb}
    elif cerrorb == "Y" : 
        dataforstan = {'npix': infilt, 'nages': nages, 'nfehs': nfehs, 'yobs': spobs, 
            'ymod': modelsb, 'yerr': spobse}                    
    modelfile = 'ultimo_modelo_22.pkl'
    fitfile = 'ultimo_ajuste_22.pkl'
    
elif itypef == "3" or itypef == "4" :
    x0 = minage-stepage/2.
    x1 = minage+stepage*nages-stepage/2.
    xages = (lages-x0)/(x1-x0)
    incage = np.zeros(nages)
    for i in range(nages):
        incage[i] = (10.**(lages[i]+stepage/2)-10.**(lages[i]-stepage/2))/1.e9

    if itypef == "3" :
        if cerrorb == "N" :
            dataforstan = {'npix': infilt, 'nages': nages, 'yobs': spobs, 
                'ymod': modelsb, 'xages': xages, 'incage': incage}
        elif cerrorb == "Y" : 
            dataforstan = {'npix': infilt, 'nages': nages, 'yobs': spobs, 
                'ymod': modelsb, 'xages': xages, 'incage': incage, 'yerr': spobse}
        modelfile = 'ultimo_modelo_31.pkl'
        fitfile = 'ultimo_ajuste_31.pkl'

    if itypef == "4" :
        if cerrorb == "N" :
            dataforstan = {'npix': infilt, 'nages': nages, 'nfehs': nfehs, 'yobs': spobs, 
                'ymod': modelsb, 'xages': xages, 'incage': incage}
        elif cerrorb == "Y" : 
            dataforstan = {'npix': infilt, 'nages': nages, 'nfehs': nfehs, 'yobs': spobs, 
                'ymod': modelsb, 'xages': xages, 'incage': incage, 'yerr': spobse}
        modelfile = 'ultimo_modelo_41.pkl'
        fitfile = 'ultimo_ajuste_41.pkl'

elif itypef == "5" or itypef == "6" :
    limage = np.zeros(nages+1)
    limage[0]=10**(lages[0]-stepage/2.)*1E-9
    for i in range(nages) :
        limage[i+1] =  10**(lages[i]+stepage/2)*1E-9
    tmin = min(limage)
    tmax = max(limage)

    if itypef == "5" :
        if cerrorb == "N" :
            dataforstan = {'npix': infilt, 'nages': nages, 'yobs': spobs, 
                'ymod': modelsb, 'limage': limage,
                'tmin': tmin, 'tmax': tmax}
        elif cerrorb == "Y" : 
            dataforstan = {'npix': infilt, 'nages': nages, 'yobs': spobs, 
                'ymod': modelsb, 'limage': limage,
                'tmin': tmin, 'tmax': tmax, 'yerr': spobse}
        modelfile = 'ultimo_modelo_51.pkl'
        fitfile = 'ultimo_ajuste_51.pkl'

    if itypef == "6" :
        if cerrorb == "N" :
            dataforstan = {'npix': infilt, 'nages': nages, 'nfehs': nfehs, 
                'yobs': spobs, 'ymod': modelsb, 'limage': limage,
                'tmin': tmin, 'tmax': tmax}
        elif cerrorb == "Y" : 
            dataforstan = {'npix': infilt, 'nages': nages, 'nfehs': nfehs, 
                'yobs': spobs, 'ymod': modelsb, 'limage': limage,
                'tmin': tmin, 'tmax': tmax, 'yerr': spobse}
        modelfile = 'ultimo_modelo_61.pkl'
        fitfile = 'ultimo_ajuste_61.pkl'

# Stan parameters. Change here if necessary:
chains = 3 # 3
warmup = 1500 #500
niter = 9500 #9500
thin = 1
n_jobs = 3



while True:
    print('Action: \n C = compile\n R = run (from previous compilation)')
    print(' S = show results (from previous run)\n T = test chains')
    print(' W = compute WAIC\n P = plots\n X = show spectra\n E = exit')
    action = raw_input('? ')

    if action == 'E': break
    
    if action == 'C':
        if itypef == "1" and cerrorb == "N" :
             modelo = StanModel(file='mcmcsp_stan_1e.stan')
        elif itypef == "1" and cerrorb == "Y" :
             modelo = StanModel(file='mcmcsp_stan_1ebar.stan')
        elif itypef == "2" and cerrorb == "N" :
              modelo = StanModel(file='mcmcsp_stan_2e.stan')
        elif itypef == "2" and cerrorb == "Y" :
              modelo = StanModel(file='mcmcsp_stan_2ebar.stan')             
        elif itypef == "3" and cerrorb == "N":
             modelo = StanModel(file='mcmcsp_stan_3e.stan')  
        elif itypef == "3" and cerrorb == "Y":
             modelo = StanModel(file='mcmcsp_stan_3ebar.stan')        
        elif itypef == "4" and cerrorb == "N":
             modelo = StanModel(file='mcmcsp_stan_4e.stan')    
        elif itypef == "4" and cerrorb == "Y":
             modelo = StanModel(file='mcmcsp_stan_4ebar.stan')       
        elif itypef == "5"  and cerrorb == "N" :
             modelo = StanModel(file='mcmcsp_stan_5e.stan') 
        elif itypef == "5"  and cerrorb == "Y" :
             modelo = StanModel(file='mcmcsp_stan_5ebar.stan')      
        elif itypef == "6"   and cerrorb == "N" :
             modelo = StanModel(file='mcmcsp_stan_6e.stan')  
        elif itypef == "6"   and cerrorb == "Y" :
             modelo = StanModel(file='mcmcsp_stan_6ebar.stan')       
        F = open(modelfile,'wb')
        pickle.dump(modelo, F)
        F.close()
        continue

    if action == 'R':

        print('Default parameters: chains = ', chains)
        print('                    warmup = ', warmup)
        print('                    iter = ', niter)
        print('                    thin = ', thin)
        print('                    n_jobs = ', n_jobs)
        try : 
            modelo
        except NameError :
            F = open(modelfile,'rb')
            modelo = pickle.load(F)
            F.close()
        if itypef == "2" and (itypemod == "1" or itypemod == "3"):
# valores iniciales
            valinit=[]
            for i in range(chains) :
                ager = 10. + np.random.random()*30.
                fehr = 2. + np.random.random()*4.
                valinit = valinit + [{'age': ager, 'feh': fehr}]
            print(valinit)
#valinit = [{'age': 20, 'feh': 2},{'age': 25, 'feh': 4},{'age': 30, 'feh': 6}]
            stanFit = modelo.sampling(data = dataforstan, chains=chains, 
                iter=niter, warmup=warmup, thin=thin, n_jobs=n_jobs,
                init=valinit)
        else :
            stanFit = modelo.sampling(data = dataforstan, chains=chains, 
                iter=niter, warmup=warmup, thin=thin, n_jobs=n_jobs)
                
        F = open(fitfile,'wb')
        pickle.dump(stanFit, F)
        F.close()
            
        continue
        
    if action == 'S' or action == 'T' or action == 'P' or action == 'W' or action == 'X':
        try : 
            stanFit
        except NameError :
            F = open(fitfile,'rb')
            stanFit = pickle.load(F)
            F.close()

    if action == 'S':
        nlines = 11 + nparameters
        output = str(stanFit).split('\n')
        for item in output[1:nlines]: print(item)
        print(' ')
        plt.ion()
        xx0 = stanFit.plot(['erry'])
        plt.pause(0.001)
#        plt.figure()
        if itypef == "1" or itypef == "2" :
            xx1 = stanFit.plot(['age'])
            plt.pause(0.001)
            if itypef == "2" :
#            plt.figure()
               xx2 = stanFit.plot(['feh'])
               plt.pause(0.001)
 
            xages = (stanFit['age'] - 1.) * stepage + minage
            meanage = np.mean(xages)
            sdage = np.std(xages, ddof=1)
            hdiage = HDIofMCMC(xages, credMass=0.95)
            print('--------------------------------')
            print('log(Age):')
            print('mean   =', meanage)
            print('sd     =', sdage)
            print('95%HDI =', hdiage) 
            print('--------------------------------')    

        if itypef == "2" or itypef == "4" or itypef == "6" : 
            xfehs = np.zeros(len(stanFit['feh']))
            for i in range(len(stanFit['feh'])) :
                xf = stanFit['feh'][i]
                bf = int(xf) - 1
                xfehs[i] = lfehs[bf]+(xf-1.-bf)*(lfehs[bf+1]-lfehs[bf])
            meanfeh = np.mean(xfehs)
            sdfeh = np.std(xfehs, ddof=1)
            hdifeh = HDIofMCMC(xfehs, credMass=0.95)
            print('--------------------------------')
            print('[Fe/H]:')
            print('mean   =', meanfeh)
            print('sd     =', sdfeh)
            print('95%HDI =', hdifeh) 
            print('--------------------------------') 

        if itypef == "3" or itypef == "4" :  
            xxages = stanFit['mu'] * (x1-x0) + x0
            meanage = np.mean(xxages)
            sdage = np.std(xxages, ddof=1)
            hdiage = HDIofMCMC(xxages, credMass=0.95)
            print('Mean age:-----------------------')
            print('log(Age):')
            print('mean   =', meanage)
            print('sd     =', sdage)
            print('95%HDI =', hdiage) 
            print('--------------------------------')             
            xx1 = stanFit.plot(['a'])
            plt.pause(0.001) 
            xx2 = stanFit.plot(['b'])
            plt.pause(0.001)  
            xx3 = stanFit.plot(['mu'])
            plt.pause(0.001) 
            xx4 = stanFit.plot(['theta'])
            plt.pause(0.001) 
            
        if itypef == "5" or itypef == "6" : 
            xt = stanFit['t']*1.E9
            xtau = stanFit['tau']*1.E9
            meant = np.mean(xt)
            mediant = np.median(xt)
            sdt = np.std(xt, ddof=1)
            hdit = HDIofMCMC(xt, credMass=0.95)
            print('--------------------------------')
            print('t:')
            print('mean   =', meant)
            print('median   =', mediant)
            print('sd     =', sdt)
            print('95%HDI =', hdit) 
            print('--------------------------------') 
            meantau = np.mean(xtau)
            mediantau = np.median(xtau)
            sdtau = np.std(xtau, ddof=1)
            hditau = HDIofMCMC(xtau, credMass=0.95)  
            print('--------------------------------')
            print('tau:')
            print('mean   =', meantau)
            print('median   =', mediantau)
            print('sd     =', sdtau)
            print('95%HDI =', hditau) 
            print('--------------------------------')  
 
            xx1 = stanFit.plot(['t'])
            plt.pause(0.001) 
            xx2 = stanFit.plot(['tau'])
            plt.pause(0.001)  
            
        if itypef == "4" or itypef == "6" :   
            xx5 = stanFit.plot(['feh'])
            plt.pause(0.001)        
                                    
        plt.show()
        

    if action == 'P':
        if itypef == "1" or itypef == "2" :
            xages = (stanFit['age'] - 1.) * stepage + minage
            meanage = np.mean(xages)
            sdage = np.std(xages, ddof=1)
            hdiage = HDIofMCMC(xages, credMass=0.95)
                       
            plt.ion()
            plt.figure()
            n, bins, pat = plt.hist(xages, bins=30, rwidth=0.90, color="mediumturquoise")
            plt.xlabel('log age')
# introduce aqui ageyr como la edad de la galaxia mock
#            ageyr = 1.E9
#            plt.plot([np.log10(ageyr),np.log10(ageyr)], [0.,max(n)],'b--')
            plt.plot([hdiage[0],hdiage[1]],[0,0],'k-',lw=10)
            text1 = 'mean = %.2f' % meanage
            plt.text(meanage,max(n), text1, ha='center',size='large')
            plt.text(hdiage[0],max(n)/30,"%.2f" % hdiage[0], ha='center',size='large')   
            plt.text(hdiage[1],max(n)/30,"%.2f" % hdiage[1], ha='center',size='large') 
            plt.text(0.5*(hdiage[0]+hdiage[1]),max(n)/30,'HDI', ha='center',size='large')  
            plt.pause(0.001)  
        
            plt.figure()
            n, bins, pat = plt.hist(xages, bins=500, range=(lages[0],lages[nages-1]),
                color="mediumturquoise" )
            plt.xlabel('log age')
#            plt.plot([np.log10(ageyr),np.log10(ageyr)], [0.,max(n)],'b--')
            for i in range(1,nages):
                plt.plot([lages[i],lages[i]], [0.,max(n)],':',color="lightgrey", lw=1)
            plt.pause(0.001)

        if itypef == "2" or itypef == "4" or itypef == "6": 
            xfehs = np.zeros(len(stanFit['feh']))
            for i in range(len(stanFit['feh'])) :
                xf = stanFit['feh'][i]
                bf = int(xf) - 1
                xfehs[i] = lfehs[bf]+(xf-1.-bf)*(lfehs[bf+1]-lfehs[bf])
            meanfeh = np.mean(xfehs)
            sdfeh = np.std(xfehs, ddof=1)
            hdifeh = HDIofMCMC(xfehs, credMass=0.95)

            plt.figure()
            n, bins, pat = plt.hist(xfehs, bins=30, rwidth=0.90, color="lightsalmon")
            plt.xlabel('[Fe/H]')
            # introduce aqui mfeh como la metalicidad de la galaxia mock
#            mfeh = 0.
#            plt.plot([mfeh,mfeh], [0.,max(n)],'b--')
            plt.plot([hdifeh[0],hdifeh[1]],[0,0],'k-',lw=10)
            text2 = 'mean = %.2f' % meanfeh
            plt.text(meanfeh,max(n), text2, ha='center',size='large')
            plt.text(hdifeh[0],max(n)/30,"%.2f" % hdifeh[0], ha='center',size='large')   
            plt.text(hdifeh[1],max(n)/30,"%.2f" % hdifeh[1], ha='center',size='large') 
            plt.text(0.5*(hdifeh[0]+hdifeh[1]),max(n)/30,'HDI', ha='center',size='large')  
            plt.pause(0.001)  
        
            plt.figure()
            n, bins, pat = plt.hist(xfehs, bins=500, range=(lfehs[0],lfehs[nfehs-1]),
                color="lightsalmon" )
            plt.xlabel('[Fe/H]')
#            plt.plot([mfeh,mfeh], [0.,max(n)],'b--')
            for i in range(1,nfehs):
                plt.plot([lfehs[i],lfehs[i]], [0.,max(n)],':',color="lightgrey", lw=1)
            plt.pause(0.001)

            if itypef == "2" :
# 2D plot (age, metallicity)            
                plt.figure()
                plt.scatter(xages, xfehs, alpha=0.1,marker='.')
#                plt.plot(np.log10(ageyr),mfeh,'r*')
                plt.plot(meanage,meanfeh,'ro')
                plt.xlabel('log(age)')
                plt.ylabel('[Fe/H]')            
                points = np.array([xages,xfehs]).T
                plot_point_cov(points, nstd=2, fc="None", ec="red")
                plt.pause(0.001)

                plt.figure()
                plt.hist2d(xages,xfehs,bins=30)
#                plt.plot(np.log10(ageyr),mfeh,'wo')
                plt.xlabel('log(age)')
                plt.ylabel('[Fe/H]')
                plt.pause(0.001)
            
                plt.figure()
                plt.hist2d(xages,xfehs,bins=500,range=[[lages[0],lages[nages-1]],
                    [lfehs[0],lfehs[nfehs-1]]])
                plt.xlabel('log(age)')
                plt.ylabel('[Fe/H]')
                for i in range(1,nages):
                    plt.plot([lages[i],lages[i]], [lfehs[0],lfehs[nfehs-1]],':'
                        ,color="grey", lw=0.5)
                for i in range(1,nfehs):
                    plt.plot([lages[0],lages[nages-1]],[lfehs[i],lfehs[i]],':'
                        , color="grey", lw=0.5)
                plot_point_cov(points, nstd=2, fc="None", ec="red") 
                plt.pause(0.001)
                            
            if itypef == "4" or itypef == "6" :
# 2D plot (mu, metallicity)   
                if itypef == "4" :
                  xmus = (stanFit['mu'])
                else :
                  xmus =(stanFit['t'])
                meanmus = np.mean(xmus)         
                plt.figure()
                plt.scatter(xmus, xfehs, alpha=0.1,marker='.')
                plt.plot(meanmus,meanfeh,'ro')
                plt.xlabel('mu')
                plt.ylabel('[Fe/H]')            
                points = np.array([xmus,xfehs]).T
                plot_point_cov(points, nstd=2, fc="None", ec="red")            

                plt.figure()
                plt.hist2d(xmus,xfehs,bins=30)
                plt.xlabel('mu')
                plt.ylabel('[Fe/H]')
            
                if itypef == "4" :
                  plt.figure()
                  plt.hist2d(xmus,xfehs,bins=500,range=[[0.,1.],
                    [lfehs[0],lfehs[nfehs-1]]])
                  plt.xlabel('mu')
                  plt.ylabel('[Fe/H]')
                  plot_point_cov(points, nstd=2, fc="None", ec="red") 
                  plt.pause(0.001)

        if itypef == "3" or itypef == "4":
            plt.ion()
            plt.figure()
            xa = stanFit['a']
            xb = stanFit['b']
            xran = np.arange(0.01,1.,0.01)
            xranp = xran*(x1-x0) + x0
            plt.plot([lages[0],lages[nages-1]],[0.,0.],'w.')
            plt.plot(xranp, beta.pdf(xran, a=np.mean(xa), b=np.mean(xb)), 'b-')
            plt.xlabel('log(age)')
            plt.ylabel('SFR')
                        
            hdisfh = np.zeros((nages,2))
            
            for i in range(nages) :
                sfh = beta.pdf(xages[i], a=xa, b=xb)
                sfhmed = np.median(sfh)
                plt.plot(lages[i],sfhmed,'ko',markersize=2.)
                hdisfh[i,] = HDIofMCMC(sfh, credMass=0.66)
#                plt.plot([xages[i],xages[i]], hdisfh[i,], 'b-')
                
            plt.plot(lages,hdisfh[:,0],'g-')
            plt.plot(lages,hdisfh[:,1],'g-')
            
# input values. Cambiar aqui.
#            plt.plot(xran,beta.pdf(xran, a=1000, b=1000), 'r-')

            plt.pause(0.001)

        if itypef == "5" or itypef == "6":
            plt.ion()
            plt.figure()
            xt = stanFit['t']*1.E9
            xtau = stanFit['tau']*1.E9
            meant = np.mean(xt)
            meantau = np.mean(xtau)
            mediant = np.median(xt)
            mediantau = np.median(xtau)
            
            trange = np.arange(10**lages[nages-1]*1E-9, 0, -0.01)
            xn = trange.size
            sfhp = np.zeros(xn)  
            for i in range(xn) :
                if (trange[i] * 1.E9 > meant) :
                    sfhp[i] = 0.
                else :
                    telapsed = meant - trange[i] * 1E9
                    sfhp[i] = telapsed * np.exp(-telapsed/meantau)
            sumsfhp = sum(sfhp)     
            sfhp = sfhp/sumsfhp*10**lages[nages-1]*1E-9*100
            plt.plot(trange, sfhp, 'b-')
            plt.xlabel('Age (Gyr)')
            plt.ylabel('SFR')
            # Now plot in green the median curve
            for i in range(xn) :
                if (trange[i] * 1.E9 > mediant) :
                    sfhp[i] = 0.
                else :
                    telapsed = mediant - trange[i] * 1E9
                    sfhp[i] = telapsed * np.exp(-telapsed/mediantau)
            sumsfhp = sum(sfhp)     
            sfhp = sfhp/sumsfhp*10**lages[nages-1]*1E-9*100
            plt.plot(trange, sfhp, 'g-')           
            

            hdisfh = np.zeros((nages,2))          
            xage = np.zeros(nages) 
            sfhmed = np.zeros(nages) 
            nchain=chains*(niter-warmup)
            fage = np.zeros(nchain) 
  
            for i in range(nages) :
                xage[i] = 10**lages[i]
                for j in range(nchain) :
                    if (xage[i] > xt[j]) :
                        fage[j] = 0.
                    else :
                        telapsed = xt[j] - xage[i]
                        fage[j] = telapsed * np.exp(-telapsed/xtau[j])    
                fage = fage/sumsfhp*1890.
                
                hdisfh[i,] = HDIofMCMC(fage, credMass=0.95)
                sfhmed[i] = np.mean(fage)
            plt.plot(xage*1E-9,sfhmed,'ko',markersize=2.)
#            plt.plot(xage*1E-9, hdisfh[:,0],'g-')
#            plt.plot(xage*1E-9, hdisfh[:,1],'g-')
            plt.pause(0.001)   

# Grafica logaritmica
            plt.figure()
            trange = np.arange(lages[0], lages[nages-1], 0.02)
            atrange = 10**trange
            xn = trange.size
            sfhp = np.zeros(xn)  
            for i in range(xn) :
                if (atrange[i] > meant) :
                    sfhp[i] = 0.
                else :
                    telapsed = meant - atrange[i]
                    sfhp[i] = telapsed * np.exp(-telapsed/meantau)
            sumsfhp = sum(sfhp)     
            sfhp = sfhp/sumsfhp*200
            plt.plot(trange, sfhp, 'b-')
            plt.xlabel('log(age)')
            plt.ylabel('SFR')
            for i in range(xn) :
                if (atrange[i] > mediant) :
                    sfhp[i] = 0.
                else :
                    telapsed = mediant - atrange[i]
                    sfhp[i] = telapsed * np.exp(-telapsed/mediantau)
            sumsfhp = sum(sfhp)     
            sfhp = sfhp/sumsfhp*200
            plt.plot(trange, sfhp, 'g-')
            plt.pause(0.001)   

            
            plt.pause(0.001)         

        plt.show()

    if action == 'T':
        la = stanFit.extract(permuted=False, inc_warmup=True)
        
        icoef = 0
        if itypef == "1" or itypef == "2" : 
            ymaxt1 = 50.
            ymaxt2 = 7.
        if itypef == "3" or itypef == "5" : 
            ymaxt1 = 1.
            ymaxt2 = 100.
        plt.ion()
        plt.figure()
        for i in range(chains):
            plt.plot(la[:,i,icoef], linewidth=1.0)
#        plt.plot([warmup,warmup],[0.,ymaxt1],'k-')
        plt.text(warmup/2,0.,'warmup',horizontalalignment='center')
#        plt.draw()
        plt.pause(0.001)
        
        icoef = 1
        plt.figure()
        for i in range(chains):
            plt.plot(la[:,i,icoef], linewidth=1.0)
#        plt.plot([warmup,warmup],[0.,ymaxt2],'k-')
        plt.text(warmup/2,0.,'warmup',horizontalalignment='center')
#        plt.draw()
        plt.pause(0.001)
        plt.show()


    if action == 'W':
# Calculo de WAIC

# Calculo de los espectros predichos para cada paso de la cadena

        nchain=chains*(niter-warmup)
        ypred = np.zeros((nchain,infilt)) 
        error = stanFit['erry']

        if itypef == "1" or itypef == "2" :

            for k in range(nchain) :
                agek = stanFit['age'][k] - 1.
                metk = 0
                if itypef == "2" : metk = stanFit['feh'][k] - 1.
                ypred[k,] = mcsp.predmodel12(itypef, infilt, agek, metk, nages, nfehs, modelsb)

        if itypef == "3" or itypef == "4" :
            x0 = minage-stepage/2.
            x1 = minage+stepage*nages-stepage/2.
            xages = (lages-x0)/(x1-x0)
            incage = np.zeros(nages)
            for i in range(nages):
                incage[i] = (10.**(lages[i]+stepage/2)-10.**(lages[i]-stepage/2))/1.e9

            for k in range(nchain) :
                xa = stanFit['a'][k]
                xb = stanFit['b'][k]
                metk = 0
                if itypef == "4" : metk = stanFit['feh'][k] - 1.
                ypred[k,] = mcsp.predmodel34(itypef, infilt, xa, xb, metk, nages, 
                    nfehs, modelsb, xages, incage)
                
        if itypef == "5" or itypef == "6" :
            limage = np.zeros(nages+1)
            limage[0]=10**(lages[0]-stepage/2.)
            for i in range(nages) :
                limage[i+1] =  10**(lages[i]+stepage/2)

            for k in range(nchain) :
                xt = stanFit['t'][k]*1.E9
                xtau = stanFit['tau'][k]*1.E9
                metk = 0
                if itypef == "6" : metk = stanFit['feh'][k] - 1.
                ypred[k,] = mcsp.predmodel56(itypef, infilt, xt, xtau, metk, nages, 
                    nfehs, modelsb, limage)

# Ahora calcula waic
                
        pr = np.zeros(infilt)
        v = np.zeros(infilt)
        for i in range(infilt) :
            print(i,spobs[i],ypred[0,i])
            vero = norm.pdf(spobs[i], loc = ypred[:,i], scale=error[:])
            pr[i] = np.mean(vero)
            v[i] =  np.var(np.log(vero))
        lppd = np.sum(np.log(pr))
        pwaic = np.sum(v)
        WAIC = -2.*(lppd - pwaic)
        print("WAIC : ", WAIC, "    pwaic : ", pwaic)
        waici = 2.*(np.log(pr) - pwaic)
        EWAIC = np.sqrt(infilt * np.var(waici))
        print("EWAIC : ", EWAIC)    

    if action == 'X':
        plt.ion()
        plt.figure()
        
        if itypemod == "1" or itypemod == "3":
            plt.plot(spobs,'k')
        elif itypemod == "2" :
#            plt.plot(spobs,'ok')
            plt.errorbar(ldo, spobs, yerr=spobse, fmt='ok')
        
        if itypef == "1" or itypef == "2" :
            agek = np.mean(stanFit['age']) - 1.
            metk = 0.
            if itypef == "2" : metk = np.mean(stanFit['feh']) - 1.
            ypred = mcsp.predmodel12(itypef, infilt, agek, metk, nages, nfehs, modelsb)        

        if itypef == "3" or itypef == "4" :
            x0 = minage-stepage/2.
            x1 = minage+stepage*nages-stepage/2.
            xages = (lages-x0)/(x1-x0)
            incage = np.zeros(nages)
            for i in range(nages):
                incage[i] = (10.**(lages[i]+stepage/2)-10.**(lages[i]-stepage/2))/1.e9
            xa = np.mean(stanFit['a'])
            xb = np.mean(stanFit['b'])
            metk = 0.
            if itypef == "4" : metk = np.mean(stanFit['feh']) - 1.
            ypred = mcsp.predmodel34(itypef, infilt, xa, xb, metk, nages, nfehs, 
                modelsb, xages, incage)                       
  
        if itypef == "5" or itypef == "6" :
            limage = np.zeros(nages+1)
            limage[0]=10**(lages[0]-stepage/2.)
            for i in range(nages) :
                limage[i+1] =  10**(lages[i]+stepage/2)
            xt = np.mean(stanFit['t'])*1.E9
            xtau = np.mean(stanFit['tau'])*1.E9
            metk = 0.
            if itypef == "6" : metk = np.mean(stanFit['feh']) - 1.
            ypred = mcsp.predmodel56(itypef, infilt, xt, xtau, metk, nages, 
                    nfehs, modelsb, limage)                               
                                                                                                                                                                                                                    
        if itypemod == "1" or itypemod == "3":
            plt.plot(ypred,'r')
        elif itypemod == "2" :
            plt.plot(ldo,ypred,'r')  
            plt.xlabel('Wavelength(microns)')    
        
        plt.pause(0.001)
        plt.show()

# stops here

#         Hay un error, al crear el especto con mcmcsp_mock y unos parámetros determinados
#         crea un espectro muy diferente del que se crea aqui con los mismos parámetros            