data {
  int<lower=0> npix ;
  int<lower=0> nages ;
  int<lower=0> nfehs ;
  vector[npix] yobs ;
  vector[npix] yerr ;
  matrix[nages*nfehs,npix] ymod ;
  vector[nages+1] limage ;
  real tmin ;
  real tmax ;
}
parameters {
  real<lower=tmin,upper=tmax> t ;
  real<lower=0.> tau ;
  real<lower=1.,upper=nfehs> feh ;
  real<lower=0.> erry ;
  vector[npix] yreal ;
}
model{
  int iz2 ;
  int j1 ;
  int j2 ;
  real weigz1 ;
  real weigz2 ;
  real suma ;
  real ta ;
  real tb ;
  vector[nages] wei ;
  vector[npix] yinterp ;
  vector[npix] yinterp1 ;
  vector[npix] yinterp2 ;

  erry ~ normal(0.,1.) ;
  tau ~ normal(0.,15.) ;

  iz2 = 1 ;
  while (iz2 < feh)
    iz2 = iz2 + 1 ;
  weigz1 = iz2 - feh ;
  weigz2 = 1. - weigz1 ;

  for (i in 1:nages) {
      ta = fmax(0., t-limage[i+1]) ;
      tb = fmax(0., t-limage[i]) ;
      wei[i] = tau*((ta+tau)*exp(-ta/tau) - (tb+tau)*exp(-tb/tau)) ;
  }

  for (j in 1:npix) {
    yinterp1[j] = 0. ;
    yinterp2[j] = 0. ;
  }

  j1 = (iz2 - 2) * nages ;
  j2 = (iz2 - 1) * nages ;
  for (i in 1:nages) {
      yinterp1 = yinterp1 + (ymod[j1+i])' * wei[i] ;
      yinterp2 = yinterp2 + (ymod[j2+i])' * wei[i] ;
  }
  yinterp = yinterp1*weigz1 + yinterp2*weigz2 ;

  suma = sum(yinterp) ;
  yinterp = yinterp/suma*npix ;
  
  yreal ~ normal(yinterp, erry) ;
  yobs ~ normal(yreal, yerr) ;
}
