# -*- coding: utf-8 -*-
from __future__ import print_function
import readline 
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.stats import norm, beta, mode
import pickle
from pystan import StanModel
from astropy.io import fits
from fjg import *
import mcmcsp_fun as mcsp
import pyphot
import extincion as ex

def mibeta(x, a, b):
    y = x**(a - 1.) * (1. - x)**(b - 1.)
    sy = sum(y)
    y = y / sy
    return y

class spectrums():

    def __init__(self):
        self.lages = 0
        self.lfehs = 0
        self.nages = 0
        self.nfehs = 0
        self.slambdab = 0
        self.modelsb = 0
        self.inflit = 0
        self.spobs = 0
        self.spobse = 0
        self.minage = 0
        self.maxage = 0
        self.stepage = 0
        self.nparameters = 0
        self.x0 = 0
        self.x1 = 0
        self.itypef = 0
        self.itypemod = 0
        self.cerrob = 0
        self.Rv=0
        self.slambdab_error=0
        self.resolve_parameters=0
        self.resolve_fv=0
        
    def atenuation(self,Rv):
        modelsRv=np.zeros([self.nages*self.nfehs*len(Rv),self.infilt])
        ii=0
        for jj in Rv:
            for i in range(self.infilt):
                for j in range(self.nages*self.nfehs):
                    modelsRv[j+ii*self.nages*self.nfehs,i]=self.modelsb[j,i]*10**(-0.4*ex.extintion(((10**4)/self.slambdab[i]),jj))
            ii+=1
        self.modelsb=modelsRv    
    def redshift(self,z):
        self.slambdab=(1+z)*self.slambdab
    def load_spectra(self, bin,red):
        if self.itypemod == "1" or self.itypemod == "3":
            # rango de edades (en log(years) de los modelos de MILES
            self.minage = 7.80
            self.maxage = 10.26
            self.stepage = 0.05
            self.lages = np.arange(self.minage, self.maxage, self.stepage)
            self.nages = self.lages.size
            # rango de metalicidades
            self.lfehs = [-2.32, -1.71, -0.31, -0.71, -0.40, 0, 0.22]
            self.nfehs = len(self.lfehs)
            if self.itypemod == "1":
                self.infilt = 4300
            elif self.itypemod == "3":
                self.infilt = 1024
        elif self.itypemod == "2":
              # rango de edades (en log(years) de los modelos de Bruzual (9 bands)
            self.minage = 6.0
            self.maxage = 10.01
            self.stepage = 0.1
            self.lages = np.arange(self.minage, self.maxage, self.stepage)
            self.nages = self.lages.size
            # rango de metalicidades
            self.lfehs = [-0.699, -0.397, 0., 0.397]
            self.nfehs = len(lfehs)
            self.infilt = 9
        elif self.itypemod == "4" or self.itypemod == "5":
            # rango de edades en gigaños
            self.lages = [0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1, 0.15, 0.20, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.25, 1.5, 1.75,
                          2.0, 2.25, 2.5, 2.75, 3.0, 3.25, 3.5, 3.75, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9., 9.5, 10., 10.5, 11.0, 11.5, 12.0, 12.5, 13.0, 13.5]
            self.nages = len(self.lages)
            # rango de metalicidades
            self.lfehs = [-2.27, -1.79, -1.49, -1.26, -0.96, -0.66, -0.35, -0.25, 0.06, 0.15, 0.26, 0.4]
            self.nfehs = len(self.lfehs)
            if self.itypemod == "4":
                slambdamin = 1680.2
                slambdamax = 49999.5
                slambdastep = 0.9
                slambda = np.arange(slambdamin, slambdamax, slambdastep)
                self.infilt = slambda.size
            elif self.itypemod == "5":
                lib = pyphot.get_library()
                list = []
                x = np.genfromtxt("Challenge-master/filters.dat", dtype=None)
                for i in x:
                    f = lib.find(i[1])
                    list.append(f)
                self.infilt = len(list)
                slambda= np.zeros(self.infilt)
                j=0
                self.slambdab_error= np.zeros([2,self.infilt])
                for w in list:
                    f = lib[w[0]]
                    self.slambdab_error[0,j]=f.cl.magnitude-(f.wavelength.magnitude).min()
                    self.slambdab_error[1,j]=(f.wavelength.magnitude).max()-f.cl.magnitude
                    slambda[j]=f.cl.magnitude
                    j+=1

        if self.itypef == "1" or self.itypef == "3" or self.itypef == "5":
            self.nfehs = 1
            self.lfehs = [0]
            if self.itypemod == "4"or self.itypemod == "5":
                self.lfehs = [0.06]
        # numero de parametros que ajustara el modelo
        if self.itypef == "1":
            self.nparameters = 1
        elif self.itypef == "2"or self.itypef == "3"or self.itypef == "5":
            self.nparameters = 2
        elif self.itypef == "4"or self.itypef == "6" or self.itypef == "7":
            self.nparameters = 3
        elif self.itypef== "8" or self.itypef=="9":
            self.nparameters= 4
        else:
            print('ERROR: Not implemented')
        models = np.zeros((self.nages * self.nfehs, self.infilt))
        # lectura de los modelos originales
        if self.itypemod == "1" or self.itypemod == "3":
            slambda = np.arange(3540.5, 7410.5, 0.9)
            dire = "MILES_SSPs/MILES_Padova00_un_1.30_fits/"
            self.slambdab = np.zeros(self.infilt)
            k = 0
            for m in range(self.nfehs):
                chmet = '%+5.2f' % self.lfehs[m]
                chmet = 'm' + chmet[1:] if self.lfehs[m] < 0 else 'p' + chmet[1:]
                print(m, chmet)
                for i in range(self.nages):
                    chage = '%07.4f' % (10**self.lages[i] * 1E-9)
                    filename = dire + "Mun1.30Z" + chmet + "T" + chage + "_iPp0.00_baseFe.fits"
                    if self.itypemod == "3":
                        filename = filename + ".usbr.fits"
                    hdul = fits.open(filename)
                    models[k, ] = hdul[0].data
                    hdul.close()
                    if self.itypef == "1" or self.itypef == "2":
                        # se normalizan
                        summodels = sum(models[k, ])
                        models[k, ] = models[k, ] / summodels * self.infilt
                    k += 1
                    
        elif self.itypemod == "2":
            modelsb = np.zeros((self.nages * self.nfehs, self.infilt))
            filebruzual = "model.BC03.bc2003.Padova1994.chab.ssp.z_2.23.flux"
            fileb = open(filebruzual, 'r')
            fileb.readline()
            if self.nfehs == 1:
                for i in range(self.nages * 2):
                    fileb.readline()
            k = 0
            for m in range(self.nfehs):
                for i in range(self.nages):
                    st = fileb.readline().split()
                    for j in range(self.infilt):
                        models[k, j] = float(st[j + 3])
                    if self.itypef == "1" or self.itypef == "2" :
                    # se normalizan
                        summodels = sum(models[k, ])
                        models[k, ] = models[k, ] / summodels * self.infilt
                    k += 1
            fileb.close()
        elif self.itypemod == "4":
            dire = "Emiles/"
            k = 0
            for m in range(self.nfehs):
                chmet = '%+5.2f' % self.lfehs[m]
                chmet = 'm' + chmet[1:] if self.lfehs[m] < 0 else 'p' + chmet[1:]
                print(m,chmet)
                for i in range(self.nages):
                    chage = '%07.4f' % (self.lages[i])
                    filename = dire + "Eku1.30Z" + chmet + "T" + chage + "_iTp0.00_baseFe.fits"
                    hdul = fits.open(filename)
                    models[k, ] = hdul[0].data
                    hdul.close()
                    if self.itypef == "1" or self.itypef == "2" :
                        # se normalizan
                        summodels = sum(models[k, ])
                        models[k, ] = models[k, ] / summodels * self.infilt
                    k += 1
         
        elif self.itypemod == "5":
            dire = "Phot_JPAS_Emiles/"
            k = 0
            for m in range(self.nfehs):
                chmet = '%+5.2f' % self.lfehs[m]
                chmet = 'm' + chmet[1:] if self.lfehs[m] < 0 else 'p' + chmet[1:]
                print(m,chmet)
                for i in range(self.nages):
                    chage = '%07.4f' % (self.lages[i])
                    filename = dire + "Eku1.30Z" + chmet + "T" + chage + "_iTp0.00_baseFe.fits"
                    hdul = fits.open(filename)
                    models[k, ] = hdul[0].data
                    hdul.close()
                    if self.itypef == "1" or self.itypef == "2":
                        # se normalizan
                        summodels = sum(models[k, ])
                        models[k, ] = models[k, ] / summodels * self.infilt
                    k +=1
        # Hacemos un binning (o no) (solo para MILES y Emiles)
        if bin <=1 or self.itypemod=="2" or self.itypemod=="5":
            self.modelsb = models
            self.slambdab = slambda
        else:
            self.infilt = int(self.infilt / bin)
            self.slambdab = np.zeros(self.infilt)
            self.modelsb = np.zeros((self.nages * self.nfehs, self.infilt))
            for i in range(self.nages * self.nfehs):
                for j in range(self.infilt):
                    j1 = j * bin
                    j2 = j1 + bin
                    self.modelsb[i, j] = np.mean(models[i, j1:j2])
            for j in range(self.infilt):
                j1 = j * bin
                j2 = j1 + bin
                self.slambdab[j] = np.mean(slambda[j1:j2])
        
        if self.itypef== "7" or self.itypef == "8" or self.itypef == "9":
            self.atenuation(self.Rv)
            if self.itypef == "7":
                for k in range(self.nages*self.nfehs*len(self.Rv)):
                    summodels = sum(self.modelsb[k, ])
                    self.modelsb[k, ] = self.modelsb[k, ] / summodels * self.infilt
        plt.figure()
        if red != 0:
            self.redshift(red)
        for m in range(self.nfehs*self.nages):
            plt.plot(self.modelsb[m,])
    plt.show()
    
    def load_observation(self, mockfile):
        #load the observation from a file.
        if self.itypemod == "1" or self.itypemod == "4" or self.itypemod == "5":
            F = open(mockfile, 'rb')
            self.spobs = pickle.load(F)
            F.close()
            print(len(self.spobs), self.infilt)
            if len(self.spobs) != self.infilt:
                print('ERROR: mock galaxy has a wrong binning')

        elif self.itypemod == "2":
            self.spobs = np.zeros(self.infilt)
            self.spobse = np.zeros(self.infilt)
            ldo = np.zeros(self.infilt)
            mockfile = "esf_13162.reg1"
            fileg = open(mockfile, 'r')
            # de momento solo lee la primera linea
            st = fileg.readline().split()
            for j in range(self.infilt):
                self.spobs[j] = float(st[(j + 1) * 5])
                ldo[j] = float(st[(j + 1) * 5 - 1]) * 1E-3  # en micras
                self.spobse[j] = float(st[(j + 1) * 5 + 1])
                # Cuidado: en el fichero de datos el 3 y el 4 están intercambiados :
            xtemp = self.spobs[4]
            self.spobs[4] = self.spobs[3]
            self.spobs[3] = xtemp
            xtemp = self.spobse[4]
            self.spobse[4] = self.spobse[3]
            self.spobse[3] = xtemp
            xtemp = ldo[4]
            ldo[4] = ldo[3]
            ldo[3] = xtemp
            summodel = sum(self.spobs)
            self.spobs = self.spobs / summodel * self.infilt
            self.spobse = self.spobse / summodel * self.infilt

        elif self.itypemod == "3":
            self.spobs = np.zeros(self.infilt)
            hdul = fits.open(mockfile)
            self.spobs[:] = hdul[0].data
            hdul.close()
            summodel = sum(self.spobs)
            self.spobs = self.spobs / summodel * self.infilt

            print(len(self.spobs), self.infilt)
            if len(self.spobs) != self.infilt:
                print('ERROR: mock galaxy has a wrong binning')
    
    def filter(self, name_of_file):
        #this filter the self.spobs and self.spobse with the filters of the file (name_of_file) (a list with the names of the filters) as long as they are in the filter library.
        #also adds the filters central lambda as self.slambdab and the filters with in self.slambdab_error
        lib = pyphot.get_library()
        list = []
        x = np.genfromtxt(name_of_file, dtype=None)
        for i in x:
            f = lib.find(i[1])
            list.append(f)
        self.infilt = len(list)
        modelobscolors = np.zeros(self.infilt)
        if self.itypef=="7" or self.itypef =="8" or self.itypef=="9":
            N=self.nages*self.nfehs*len(self.Rv)
        else :
            N=self.nages*self.nfehs
        modelsbcolors = np.zeros((N, self.infilt))
        for i in range(N):
            j = 0
            for w in list:
                f = lib[w[0]]
                modelsbcolors[i, j] = f.get_flux(self.slambdab, self.modelsb[i, ], axis=-0)
                j += 1
        j = 0
        self.slambdab_error= np.zeros([2,self.infilt])
        #addding error bars with the length of the width of the filters.
        for w in list:
            f = lib[w[0]]
            self.slambdab_error[0,j]=f.cl.magnitude-(f.wavelength.magnitude).min()
            self.slambdab_error[1,j]=(f.wavelength.magnitude).max()-f.cl.magnitude
            modelobscolors[j] = f.get_flux(self.slambdab, self.spobs, axis=-0)
            j += 1
        #change lambdab to the center of each filter.
        self.slambdab= np.zeros(self.infilt)
        j=0
        for w in list:
            f = lib[w[0]]
            self.slambdab[j]=f.cl.magnitude
            j+=1
        self.spobs = modelobscolors
        self.modelsb = modelsbcolors

    def save_spectrum_as_fits(self, dire):
        #to save the filter photometry to avoid recalculation TODO we need to add Rv for saving the attenuation part.
        k = 0
        for m in range(self.nfehs):
            chmet = '%+5.2f' % self.lfehs[m]
            chmet = 'm' + chmet[1:] if self.lfehs[m] < 0 else 'p' + chmet[1:]
            for i in range(self.nages):
                chage = '%07.4f' % (self.lages[i])
                filename = dire + "Eku1.30Z" + chmet + "T" + chage + "_iTp0.00_baseFe.fits"
                hdu = fits.PrimaryHDU(self.modelsb[k, ])
                hdu.writeto(filename)
                k += 1

    def create_model(self, out_file,parameters):
        #create a model(self. with the spectra load ) parameters the parameters for the model .
        modelinter2 = np.zeros((self.nfehs*self.nages, self.infilt))
        modelinter = np.zeros((self.nfehs, self.infilt))
        modelobs = np.zeros(self.infilt)
        if self.itypef == "7" or self.itypef =="8" or self.itypef =="9":
            Rv=parameters["Rv"]
            if (Rv < 2.0 or Rv > 5.0):
                print('ERROR: Rv out of range')
            jj=0
            while Rv > self.Rv[jj + 1]:
                jj += 1
            i1 = jj
            i2 = i1 + 1
            weight1 = (self.Rv[i2] - Rv) / (self.Rv[i2] - self.Rv[i1])
            weight2 = 1. - weight1
            for m in range(len(self.lages*self.nfehs)):
                i1m = m + self.nages * self.nfehs*i1
                i2m =m + self.nages * self.nfehs*i2
                modelinter2[m, ] = self.modelsb[i1m, ] * weight1 + self.modelsb[i2m, ] * weight2
        else:
            modelinter2=self.modelsb
        if self.itypemod == "1" or self.itypemod == "3":
        
            if self.itypef == "1" or self.itypef == "2" or self.itypef == "7":
                ageyr=parameters['ageyr']
                #ageyr = float(raw_input('Mock galaxy age (in years) (0.0631E9-17.7828E9)? '))
                if (ageyr < 0.0631E9 or ageyr > 17.7828E9):
                    print('ERROR: age out of range')
                lageyr = (np.log10(ageyr) - self.minage) / self.stepage
                if self.itypef == "2" or self.itypef == "7":
                   mfeh=parameters['mfeh']
                   #mfeh = float(raw_input('Metallicity (in [Fe/H]) (-2.32 - +0.22)? '))
                   if (mfeh < -2.32 or mfeh > 0.22):
                       print('ERROR: metallicity out of range')
                if self.itypef == "1" or self.itypef == "2" or self.itypef == "7": ## nedd change but we leave it like this for now
                    i1 = int(np.floor(lageyr))
                    i2 = i1 + 1
                    weight1 = i2 - lageyr
                    weight2 = 1. - weight1
                    for m in range(self.nfehs):
                        for j in range(self.infilt):
                            i1m = i1 + self.nages * m
                            i2m = i1m + 1
                            #print(i1m,i2m)
                            modelinter[m, j] = modelinter2[i1m, j] * weight1 + modelinter2[i2m, j] * weight2
                    if self.itypef == "1":
                        modelobs = modelinter[0, ]
                    else:
                        jj = 0
                        while mfeh > self.lfehs[jj + 1]:
                            jj += 1
                        i1 = jj
                        i2 = i1 + 1
                        weight1 = (self.lfehs[i2] - mfeh) / (self.lfehs[i2] - self.lfehs[i1])
                        weight2 = 1. - weight1
                        for j in range(self.infilt):
                            modelobs[j] = modelinter[i1, j] * weight1 + modelinter[i2, j] * weight2
            
            if self.itypef == "3" or self.itypef == "4" or self.itypef=="8":
                #a = float(raw_input('Enter "a" parameter? '))
                a=parameters['a']
                #b = float(raw_input('Enter "b" parameter? '))
                b=parameters['b']
                self.x0 = self.minage - self.stepage / 2.
                self.x1 = self.minage + self.stepage * self.nages - self.stepage / 2.
                xages = (self.lages - self.x0) / (self.x1 - self.x0)
                sfh = mibeta(xages, a, b)
                plt.ion()
                plt.figure()
                plt.plot(self.lages, sfh, 'b-')
                plt.plot(self.lages, sfh, 'k.')
                plt.pause(0.001)
                plt.show()

                incage = np.zeros(self.nages)
                for i in range(self.nages):
                    incage[i] = (10.**(self.lages[i] + self.stepage / 2) - 10. **(self.lages[i] - self.stepage / 2)) / 0.e9
                if self.itypef == "4" or self.itypef=="8":
                    mfeh=parameters['mfeh']
                    #mfeh = float(raw_input('Metallicity (in [Fe/H]) (-2.32 - +0.22)? '))
                    if (mfeh < -2.32 or mfeh > 0.22):
                        print('ERROR: metallicity out of range')

                for m in range(self.nfehs):
                    for i in range(self.nages):
                        for j in range(self.infilt):
                            im = i + self.nages * m
                            modelinter[m, j] = modelinter[m, j] + modelinter2[im, j] * sfh[i] * incage[i]

                if self.itypef == "3":
                    modelobs = modelinter[0, ]
                else:
                    jj = 0
                    while mfeh > self.lfehs[jj + 1]:
                        jj += 1
                    i1 = jj
                    i2 = i1 + 1
                    weight1 = (self.lfehs[i2] - mfeh) /(self.lfehs[i2] - self.lfehs[i1])
                    weight2 = 1. - weight1
                    for j in range(self.infilt):
                        modelobs[j] = modelinter[i1, j] * weight1 + modelinter[i2, j] * weight2

            if self.itypef == "5" or self.itypef == "6" or self.itypef=="9":
                t0yr=parameters['t0yr']
                #t0yr = float(raw_input('t parameter (in years) (0.06E9-18.9E9)? '))
                if (t0yr < 0.06E9 or t0yr > 18.9E9):
                    print('ERROR: t out of range')
                tauyr=parameters['tauyr']
                #tauyr = float(raw_input('tau parameter (in years) (>0)? '))
                if (tauyr < 0.0):
                    print('ERROR: tau out of range')
                if self.itypef == "6":
                    mfeh=parameters['mfeh']
                    #mfeh = float(raw_input('Metallicity (in [Fe/H]) (-2.32 - +0.22)? '))
                    if (mfeh < -2.32 or mfeh > 0.22):
                        print('ERROR: metallicity out of range')

                trange = np.arange(18.9, 0, -0.01)
                xn = trange.size
                sfhp = np.zeros(xn)
                for i in range(xn):
                    if (trange[i] * 1.E9 > t0yr):
                        sfhp[i] = 0.
                    else:
                        telapsed = t0yr - trange[i] * 1E9
                        sfhp[i] = telapsed * np.exp(-telapsed / tauyr)
                sumsfhp = sum(sfhp)
                sfhp = sfhp / sumsfhp * 1890.
                plt.ion()
                plt.figure()
                plt.plot(trange, sfhp, 'b-')

                xage = np.zeros(self.nages)
                limage = np.zeros(self.nages + 1)
                limage[0] = 10**7.775
                wei = np.zeros(self.nages)
                fage = np.zeros(self.nages)
                for i in range(self.nages):
                    xage[i] = 10**self.lages[i]
                    limage[i + 1] = 10**(self.lages[i] + self.stepage / 2)
                    if (xage[i] > t0yr):
                        fage[i] = 0.
                    else:
                        telapsed = t0yr - xage[i]
                        fage[i] = telapsed * np.exp(-telapsed / tauyr)

                    ta = max(0., t0yr - limage[i + 1])
                    tb = max(0., t0yr - limage[i])
                    wei[i] = tauyr * ((ta + tauyr) * np.exp(-ta / tauyr) -(tb + tauyr) * np.exp(-tb / tauyr))

                #     plt.plot([limage[i+1]*1E-9,limage[i+1]*1E-9],[0.,10.],'--')
                weit = sum(wei)
                wei = wei / weit
                fage = fage / sumsfhp * 1890.
                plt.plot(xage * 1E-9, fage, 'k.')
                plt.pause(0.001)
                plt.show()

                for m in range(self.nfehs):
                    for i in range(self.nages):
                        for j in range(self.infilt):
                            im = i + self.nages * m
                            modelinter[m, j] = modelinter[m, j] + modelinter2[im, j] * wei[i]

                if self.itypef == "5":
                    modelobs = modelinter[0, ]
                else:
                    jj = 0
                    while mfeh > self.lfehs[jj + 1]:
                        jj += 1
                    i1 = jj
                    i2 = i1 + 1
                    weight1 = (self.lfehs[i2] - mfeh) / (self.lfehs[i2] - self.lfehs[i1])
                    weight2 = 1. - weight1
                    for j in range(self.infilt):
                        modelobs[j] = modelinter[i1, j] * weight1 + modelinter[i2, j] * weight2
#----------------------------------------------------------------------------------------------------#------------------------------------------------------------------------------#----------------------------------------------------------------------------------------------------#------------------------------------------------------------------------------

        elif self.itypemod == "2":
            print("not implemented yet")
            exit()
#----------------------------------------------------------------------------------------------------#------------------------------------------------------------------------------#----------------------------------------------------------------------------------------------------#------------------------------------------------------------------------------

        elif self.itypemod == "4"or self.itypemod == "5":
            if self.itypef == "1" or self.itypef == "2" or self.itypef=="7":
                ageyr=parameters['ageyr']
                if (ageyr < 0.03 or ageyr > 13.5):
                    print('ERROR: age out of range')
                if self.itypef == "2" or self.itypef=="7":
                    mfeh=parameters['mfeh']
                    if (mfeh < -2.27 or mfeh > 0.4):
                        print('ERROR: metallicity out of range')
                jj = 0
                while ageyr > self.lages[jj + 1]:
                    jj += 1
                i1 = jj
                i2 = i1 + 1
                weight1 = (self.lages[i2] - ageyr) /  (self.lages[i2] - self.lages[i1])
                weight2 = 1. - weight1
                for m in range(self.nfehs):
                    for j in range(self.infilt):
                        i1m = i1 + self.nages * m
                        i2m = i1m + 1
                        modelinter[m, j] = modelinter2[i1m, j] * weight1 + modelinter2[i2m, j] * weight2

                if self.itypef == "1":
                    modelobs = modelinter[0, ]
                else:
                    jj = 0
                    while mfeh > self.lfehs[jj + 1]:
                        jj += 1
                    i1 = jj
                    i2 = i1 + 1
                    weight1 = (self.lfehs[i2] - mfeh) / (self.lfehs[i2] - self.lfehs[i1])
                    weight2 = 1. - weight1

                    for j in range(self.infilt):
                        modelobs[j] = modelinter[i1, j] * weight1 + modelinter[i2, j] * weight2
            if self.itypef == "3" or self.itypef == "4" or self.itypef== "8":
                a=parameters['a']
                b=parameters['b']

                self.x0 = self.lages[0] - (self.lages[1] - self.lages[0]) / 2.
                self.x1 = self.lages[self.nages - 1] +(self.lages[self.nages - 1] -self.lages[self.nages - 2]) / 2.
                self.lages = np.asarray(self.lages)
                xages = (self.lages - self.x0) / (self.x1 - self.x0)
                sfh = mibeta(xages, a, b)
                plt.ion()
                plt.figure()
                plt.plot(self.lages, sfh, 'b-')
                plt.plot(self.lages, sfh, 'k.')
                plt.pause(0.001)
                plt.show()

                incage = np.zeros(self.nages)
                for i in range(self.nages):
                    if i == self.nages - 1:
                        incage[i] = self.lages[i] - self.lages[i - 1]
                    else:
                        incage[i] = self.lages[i + 1] - self.lages[i]

                if self.itypef == "4" or self.itypef == "8":
                    mfeh=parameters['mfeh']
                    if (mfeh < -2.27 or mfeh > 0.4):
                        print('ERROR: metallicity out of range')
                for m in range(self.nfehs):
                    for i in range(self.nages):
                        for j in range(self.infilt):
                            im = i + self.nages * m
                            modelinter[m, j] = modelinter[m, j] + modelinter2[im, j] * sfh[i] * incage[i]

                if self.itypef == "3":
                    modelobs = modelinter[0, ]
                else:
                    jj = 0
                    while mfeh > self.lfehs[jj + 1]:
                        jj += 1
                    i1 = jj
                    i2 = i1 + 1
                    weight1 = (self.lfehs[i2] - mfeh) / (self.lfehs[i2] - self.lfehs[i1])
                    weight2 = 1. - weight1
                    for j in range(self.infilt):
                        modelobs[j] = modelinter[i1, j] * weight1 + modelinter[i2, j] * weight2

            if self.itypef == "5" or self.itypef == "6" or self.itypef=="9":
                t0yr=parameters['t0yr']
                if (t0yr < 0.03 or t0yr > 14.0):
                    print('ERROR: t out of range')
                tauyr=parameters['tauyr']
                if (tauyr < 0.0):
                    print('ERROR: tau out of range')
                if self.itypef == "6" or self.itypef=="9":
                    mfeh=parameters['mfeh']
                    if (mfeh < -2.32 or mfeh > 0.22):
                        print('ERROR: metallicity out of range')

                trange = np.arange(14.0, 0, -0.01)
                xn = trange.size
                sfhp = np.zeros(xn)
                for i in range(xn):
                    if (trange[i] * 1. > t0yr):
                        sfhp[i] = 0.
                    else:
                        telapsed = t0yr - trange[i] 
                        sfhp[i] = telapsed * np.exp(-telapsed / tauyr)
                        sumsfhp = sum(sfhp)
                        sfhp = sfhp / sumsfhp * 1890.
                plt.ion()
                plt.figure()
                plt.plot(trange, sfhp, 'b-')

                xage = np.zeros(self.nages)
                limage = np.zeros(self.nages + 1)
                limage[0] = self.lages[0]
                wei = np.zeros(self.nages)
                fage = np.zeros(self.nages)
                for i in range(self.nages):
                    xage[i] = self.lages[i]
                    limage[i + 1] = self.lages[i] +(self.lages[i] -self.lages[i - 1]) / 2.
                    if (xage[i] > t0yr):
                        fage[i] = 0.
                    else:
                        telapsed = t0yr - xage[i]
                        fage[i] = telapsed * np.exp(-telapsed / tauyr)

                    ta = max(0., t0yr - limage[i + 1])
                    tb = max(0., t0yr - limage[i])
                    wei[i] = tauyr * ((ta + tauyr) * np.exp(-ta / tauyr) - (tb + tauyr) * np.exp(-tb / tauyr))

                #     plt.plot([limage[i+1]*1E-9,limage[i+1]*1E-9],[0.,10.],'--')
                weit = sum(wei)
                wei = wei / weit
                fage = fage / sumsfhp * 1890.
                #plt.plot(xage , fage, 'k.')
                #plt.pause(0.001)
                #plt.show()

                for m in range(self.nfehs):
                    for i in range(self.nages):
                        for j in range(self.infilt):
                            im = i + self.nages * m
                            modelinter[m, j] = modelinter[m, j] + \
                                modelinter2[im, j] * wei[i]

                if self.itypef == "5":
                    modelobs = modelinter[0, ]
                else:
                    jj = 0
                    while mfeh > self.lfehs[jj + 1]:
                        jj += 1
                    i1 = jj
                    i2 = i1 + 1
                    weight1 = (self.lfehs[i2] - mfeh) / (self.lfehs[i2] - self.lfehs[i1])
                    weight2 = 1. - weight1
                    for j in range(self.infilt):
                        modelobs[j] = modelinter[i1, j] * weight1 + modelinter[i2, j] * weight2
#----------------------------------------------------------------------------------------------------#------------------------------------------------------------------------------#----------------------------------------------------------------------------------------------------#------------------------------------------------------------------------------
        # se normaliza        
        summodel = sum(modelobs)
        modelobs = modelobs / summodel * self.infilt
        error=parameters["error"]
        spobs = norm.rvs(modelobs, error, size=self.infilt)
        F = open(out_file, 'wb')
        pickle.dump(spobs, F)
        F.close()

    def data_stan(self):
        # Datos para Stan:
        fitfile = 'ultimo_ajuste_' +"%c" % self.itypef+"1.pkl"
        modelfile = 'ultimo_modelo_' +"%c" % self.itypef+"1.pkl"
        if self.itypef == "1":
            dataforstan = {'npix': self.infilt, 'nages': self.nages, 'yobs': self.spobs,'ymod': self.modelsb}
        elif self.itypef == "2":
            dataforstan = {'npix': self.infilt, 'nages': self.nages,'nfehs': self.nfehs, 'yobs': self.spobs, 'ymod': self.modelsb}
        elif self.itypef == "7":
            dataforstan = {'npix': self.infilt, 'nages': self.nages, 'nfehs': self.nfehs,'nRv':len(self.Rv),'yobs': self.spobs, 'ymod': self.modelsb}
        elif self.itypef == "3" or self.itypef == "4" or self.itypef=="8":
            if self.itypemod == "4"or self.itypemod == "5":
                self.lages = np.asarray(self.lages)
                self.x0 = self.lages[0] - (self.lages[1] - self.lages[0]) / 2.
                self.x1 = self.lages[self.nages - 1] + (self.lages[self.nages - 1] -self.lages[self.nages - 2]) / 2.
                xages = (self.lages - self.x0) / (self.x1 - self.x0)
                incage = np.zeros(self.nages)
                sfh = np.zeros(self.nages)
                for i in range(self.nages):
                    if i == self.nages - 1:
                        incage[i] = self.lages[i] - self.lages[i - 1]
                    else:
                        incage[i] = self.lages[i + 1] - self.lages[i]
                    sfh[i] = (xages[i]**(1.2 - 1)) * (1. - xages[i])**(2.1 - 1)
            elif self.itypemod == "1" or self.itypemod == "3":
                self.x0 = self.minage - self.stepage / 2.
                self.x1 = self.minage + self.stepage * self.nages - self.stepage / 2.
                xages = (self.lages - self.x0) / (self.x1 - self.x0)
                incage = np.zeros(self.nages)
                for i in range(self.nages):
                    incage[i] = (10.**(self.lages[i] + self.stepage / 2) - 10. **(self.lages[i] - self.stepage / 2)) / 1.e9
            if self.itypef == "3":
                dataforstan = {'npix': self.infilt, 'nages': self.nages, 'yobs': self.spobs,'ymod': self.modelsb, 'xages': xages, 'incage': incage}
            elif self.itypef == "4":
                dataforstan = {'npix': self.infilt, 'nages': self.nages, 'nfehs': self.nfehs,'yobs': self.spobs, 'ymod': self.modelsb, 'xages': xages, 'incage': incage}
            elif self.itypef == "8":
                dataforstan = {'npix': self.infilt, 'nages': self.nages, 'nfehs': self.nfehs, 'nRv':len(self.Rv),'yobs': self.spobs, 'ymod': self.modelsb, 'xages': xages, 'incage': incage}
                
        elif self.itypef == "5" or self.itypef == "6" or self.itypef=="9":
            limage = np.zeros(self.nages + 1)
            if self.itypemod == "1" or self.itypemod == "3":
                limage[0] = 10**(self.lages[0] - self.stepage / 2.) * 1E-9
                for i in range(self.nages):
                    limage[i + 1] = 10**(self.lages[i] + self.stepage / 2) * 1E-9
            elif self.itypemod == "2":
                print("not implemented yet")
                exit()
            elif self.itypemod == "4"or self.itypemod == "5":
                limage[0] = self.lages[0] -(self.lages[1] - self.lages[0]) / 2.
                for i in range(self.nages):
                    limage[i + 1] = self.lages[i] - (self.lages[i] - self.lages[i - 1]) / 2.
            tmin = min(limage)
            tmax = max(limage)
            if self.itypef == "5":
                dataforstan = {'npix': self.infilt, 'nages': self.nages, 'yobs': self.spobs,'ymod': self.modelsb, 'limage': limage, 'tmin': tmin, 'tmax': tmax}
            if self.itypef=="6":
                dataforstan = {'npix': self.infilt, 'nages': self.nages, 'nfehs': self.nfehs, 'yobs': self.spobs,'ymod': self.modelsb, 'limage': limage, 'tmin': tmin, 'tmax': tmax}
            if self.itypef=="9":
                print(tmin)
                dataforstan = {'npix': self.infilt, 'nages': self.nages, 'nfehs': self.nfehs, 'nRv':len(self.Rv),'yobs': self.spobs,'ymod': self.modelsb, 'limage': limage, 'tmin': tmin, 'tmax': tmax}
        if self.cerrob == "Y":
            dataforstan['yerr']=self.spobse
        return {'1': dataforstan, '2': modelfile, '3': fitfile}

    def run_stan(self, dataforstan, modelfile, fitfile, stanparameters,mock_parameters,action):
        nparameters = self.nparameters
        chains = stanparameters["chains"]
        warmup = stanparameters["warmup"]
        niter = stanparameters["niter"]
        thin = stanparameters["thin"]
        n_jobs = stanparameters["n_jobs"]
        count=0
        T=True
        while T:
            if action !="0" and count ==0:
                T=False
            else:
                count = 1
                print('Action: \n C = compile\n R = run (from previous compilation)\n S = show results (from previous run)\n T = test chains \n W = compute WAIC \n P = plots\n X = show spectra\n E = exit')
                action = raw_input('? ')                
            if action == 'E':
                break
            if action == 'C':
                mek = 'mcmcsp_stan_' +"%c" % self.itypef
                if self.cerrob == "N":
                    modelo = StanModel(file=mek + 'e.stan')
                elif self.cerrob == "Y" :
                    modelo = StanModel(file=mek + 'ebar.stan')
                F = open(modelfile, 'wb')
                pickle.dump(modelo, F)
                F.close()
                continue
            if action == 'R':
                print('Default parameters: chains = ', chains,'\n                    warmup = ', warmup,'\n                    iter = ', niter,'\n                    thin = ', thin,'\n             n_jobs = ', n_jobs)
                try:
                    modelo
                except NameError:
                    F = open(modelfile, 'rb')
                    modelo = pickle.load(F)
                    F.close()
                if self.itypef == "2" and (self.itypemod == "1" or self.itypemod == "3" or self.itypemod == "4"or self.itypemod == "5"):
                    # valores iniciales
                    valinit = []
                    for i in range(chains):
                        ager = 10. + np.random.random() * 30.
                        fehr = 2. + np.random.random() * 4.
                        valinit = valinit + [{'age': ager, 'feh': fehr}]
                    print(valinit)
                    # valinit = [{'age': 20, 'feh': 2},{'age': 25, 'feh': 4},{'age': 30, 'feh': 6}]
                    stanFit = modelo.sampling(data=dataforstan, chains=chains, iter=niter,warmup=warmup, thin=thin, n_jobs=n_jobs, init=valinit)
                else:
                    stanFit = modelo.sampling(data=dataforstan, chains=chains, iter=niter, warmup=warmup, thin=thin, n_jobs=n_jobs)
                self.resolve_parameters=stanFit
                F = open(fitfile, 'wb')
                pickle.dump(stanFit, F)
                F.close()
                continue
            if action == 'S' or action=='SP' or action == 'T' or action == 'P' or action == 'W' or action == 'X':
                try:
                    stanFit
                except NameError:
                    F = open(fitfile, 'rb')
                    stanFit = pickle.load(F)
                    F.close()
            if action =="SP":
                print(stanFit.stansummary())
                stanFit.plot()
                plt.figure()
                extract=stanFit.extract()
                while extract:
                    key=extract.popitem(False)[0]
                    plt.figure()
                    plt.hist(stanFit[key],bins='auto')
            if action == 'S':
                print(stanFit.stansummary())
                plt.ion()
                xx0 = stanFit.plot(['erry'])
                plt.pause(0.001)
        #        plt.figure()
                if self.itypef == "1" or self.itypef == "2" or self.itypef == "7":
                    xx1 = stanFit.plot(['age'])
                    plt.pause(0.001)
                    if self.itypef == "2":
                        xx2 = stanFit.plot(['feh'])
                        plt.pause(0.001)
                    if self.itypemod == "1" or self.itypemod == "3"or self.itypemod =="4" or self.itypemod == "5":
                        if self.itypemod == "1" or self.itypemod =="3":
                            xages = (stanFit['age'] - 0.) * self.stepage + self.minage
                        elif self.itypemod =="4" or self.itypemod =="5":
                            xages = np.zeros(len(stanFit['age']))
                            for i in range(len(stanFit['age'])):
                                xf = stanFit['age'][i]
                                bf = int(xf) - 1
                                xages[i] = self.lages[bf] +(xf - 1. - bf) * (self.lages[bf + 1] - self.lages[bf])
                        meanage = np.mean(xages)
                        sdage = np.std(xages, ddof=1)
                        hdiage = HDIofMCMC(xages, credMass=0.95)
                        print('--------------------------------\n log(Age):\n mean   =', meanage,'\n sd     =',sdage,'\n 95%HDI =', hdiage,'\n--------------------------------')
                    elif self.itypemod == "2":
                        print("not implemented yet ")

                if self.itypef == "2" or self.itypef == "4" or self.itypef == "6" or self.itypef=="7" or self.itypef =="8":
                    xfehs = np.zeros(len(stanFit['feh']))
                    for i in range(len(stanFit['feh'])):
                        xf = stanFit['feh'][i]
                        bf = int(xf) - 1
                        xfehs[i] = self.lfehs[bf] + (xf - 1. - bf) * (self.lfehs[bf + 1] - self.lfehs[bf])
                    meanfeh = np.mean(xfehs)
                    sdfeh = np.std(xfehs, ddof=1)
                    hdifeh = HDIofMCMC(xfehs, credMass=0.95)
                    print('--------------------------------\n[Fe/H]:\nmean   =', meanfeh,'\n sd     =', sdfeh,'\n 95%HDI =', hdifeh,'\n--------------------------------')

                if self.itypef == "3" or self.itypef == "4" or self.itypef =="8":
                    xxages = stanFit['mu'] * (self.x1 - self.x0) + self.x0
                    meanage = np.mean(xxages)
                    sdage = np.std(xxages, ddof=1)
                    hdiage = HDIofMCMC(xxages, credMass=0.95)
                    print('--------------------------------\n log(Age):\n mean   =', meanage,'\n sd     =',sdage,'\n 95%HDI =', hdiage,'\n--------------------------------')
                    xx1 = stanFit.plot(['a'])
                    plt.pause(0.001)
                    xx2 = stanFit.plot(['b'])
                    plt.pause(0.001)
                    xx3 = stanFit.plot(['mu'])
                    plt.pause(0.001)
                    xx4 = stanFit.plot(['theta'])
                plt.pause(0.001)

                if self.itypef == "5" or self.itypef == "6" or self.itypef=="9":
                    xt = stanFit['t'] 
                    xtau = stanFit['tau'] 
                    meant = np.mean(xt)
                    mediant = np.median(xt)
                    sdt = np.std(xt, ddof=1)
                    hdit = HDIofMCMC(xt, credMass=0.95)
                    print('--------------------------------\nt:\nmean   =', meant,'\n median   =', mediant,'\n sd     =', sdt,'\n 95%HDI =', hdit,'\n--------------------------------')
                    meantau = np.mean(xtau)
                    mediantau = np.median(xtau)
                    sdtau = np.std(xtau, ddof=1)
                    hditau = HDIofMCMC(xtau, credMass=0.95)
                    print('--------------------------------\ntau:\nmean   =', meantau,'\n median   =', mediantau,'\n sd     =', sdtau,'\n 95%HDI =', hditau,'\n------------------------------')
                    xx1 = stanFit.plot(['t'])
                    plt.pause(0.001)
                    xx2 = stanFit.plot(['tau'])
                    plt.pause(0.001)

                if self.itypef == "4" or self.itypef == "6"or self.itypef=="8":
                    xx5 = stanFit.plot(['feh'])
                    plt.pause(0.001)

                plt.show()
            if action == 'P':
                #make mock parameters = False when we are not coming from a mock galaxy
                if mock_parameters==0:
                    print("not_mock_galaxy")
                else:
                    ageyr=mock_parameters["ageyr"]
                    mfeh=mock_parameters["mfeh"]
                    a=mock_parameters["a"]
                    b=mock_parameters["b"]
                    t0yr=mock_parameters["t0yr"]
                    tauyr=mock_parameters["tauyr"]
                    error=mock_parameters["error"]
                    mu_mock=a/(a+b)
                    theta_mock=a+b
                    if self.itypemod=="1" or self.itypemod=="3":
                        t0yr=t0yr/1E9
                        tauyr=tauyr/1E9
                if self.itypef == "1" or self.itypef == "2":
                    xages = (stanFit['age'] - 1.) * self.stepage + self.minage
                    if self.itypemod == "4" or self.itypemod == "5":
                        xages = np.zeros(len(stanFit['age']))
                        for i in range(len(stanFit['age'])):
                            xf = stanFit['age'][i]
                            bf = int(xf) - 1
                            xages[i] = self.lages[bf] +(xf - 1. - bf) * (self.lages[bf + 1] - self.lages[bf])
        
                    meanage = np.mean(xages)
                    sdage = np.std(xages, ddof=1)
                    hdiage = HDIofMCMC(xages, credMass=0.95)
                    plt.ion()
                    plt.figure()
                    n, bins, pat = plt.hist(xages, bins=30, rwidth=0.90, color="mediumturquoise")
                    if self.itypemod == "4" or self.itypemod == "5":
                        plt.xlabel('Age(Gyr)')
                        if mock_parameters!=0:
                            plt.plot([ageyr,ageyr], [0.,max(n)],'b--')
                    if self.itypemod == "1" or self.itypemod == "3":
                        plt.xlabel('log(Age(yr))')
                        if mock_parameters!=0:
                            plt.plot([np.log10(ageyr),np.log10(ageyr)], [0.,max(n)],'b--')
                    plt.plot([hdiage[0], hdiage[1]], [0, 0], 'k-', lw=10)
                    text1 = 'mean = %.2f' % meanage
                    plt.text(meanage, max(n), text1, ha='center', size='large')
                    plt.text(hdiage[0], max(n) / 30, "%.2f" % hdiage[0], ha='center', size='large')
                    plt.text(hdiage[1], max(n) / 30, "%.2f" % hdiage[1], ha='center', size='large')
                    plt.text(0.5 * (hdiage[0] + hdiage[1]),max(n) / 30, 'HDI', ha='center', size='large')
                    plt.pause(0.001)
                    
                    plt.figure()
                    n, bins, pat = plt.hist(xages, bins=500, range=(self.lages[0], self.lages[self.nages - 1]),color="mediumturquoise")
                    if self.itypemod == "4" or self.itypemod == "5":
                        plt.xlabel('Age(Gyr)')
                        if mock_parameters!=0:
                            plt.plot([ageyr,ageyr], [0.,max(n)],'b--')
                    if self.itypemod == "1" or self.itypemod == "3":
                        plt.xlabel('log(Age(yr))')
                        if mock_parameters!=0:
                            plt.plot([np.log10(ageyr),np.log10(ageyr)], [0.,max(n)],'b--')
                    for i in range(1, self.nages):
                        plt.plot([self.lages[i], self.lages[i]], [0., max(n)],':', color="lightgrey", lw=1)
                    plt.pause(0.001)

                if self.itypef == "2" or self.itypef == "4" or self.itypef == "6" or self.itypef == "8":
                    xfehs = np.zeros(len(stanFit['feh']))
                    for i in range(len(stanFit['feh'])):
                        xf = stanFit['feh'][i]
                        bf = int(xf) - 1
                        xfehs[i] = self.lfehs[bf] + (xf - 1. - bf) *(self.lfehs[bf + 1] - self.lfehs[bf])
                    meanfeh = np.mean(xfehs)
                    sdfeh = np.std(xfehs, ddof=1)
                    hdifeh = HDIofMCMC(xfehs, credMass=0.95)
                    if self.itypef == "4" or self.itypef=="8":
                        xa=stanFit["a"]
                        xb=stanFit["b"]
                        meana = np.mean(xa)
                        sda = np.std(xa, ddof=1)
                        hdia = HDIofMCMC(xa, credMass=0.95)
                        meanb = np.mean(xb)
                        sdb = np.std(xb, ddof=1)
                        hdib = HDIofMCMC(xb, credMass=0.95)
                        
                        plt.figure()
                        n, bins, pat = plt.hist(xa, bins=30, rwidth=0.90, color="mediumturquoise")
                        plt.xlabel('a')
                        if mock_parameters!=0:
                            plt.plot([a,a], [0.,max(n)],'b--')
                        plt.plot([hdia[0], hdia[1]], [0, 0], 'k-', lw=10)
                        text1 = 'mean = %.2f' % meana
                        plt.text(meana, max(n), text1, ha='center', size='large')
                        plt.text(hdia[0], max(n) / 30, "%.2f" % hdia[0], ha='center', size='large')
                        plt.text(hdia[1], max(n) / 30, "%.2f" % hdia[1], ha='center', size='large')
                        plt.text(0.5 * (hdia[0] + hdia[1]),max(n) / 30, 'HDI', ha='center', size='large')
                        plt.pause(0.001)

                        plt.figure()
                        plt.ion()
                        n, bins, pat = plt.hist(xb, bins=30, rwidth=0.90, color="mediumturquoise")
                        if mock_parameters!=0:
                            plt.plot([b,b], [0.,max(n)],'b--')
                        plt.plot([hdib[0], hdib[1]], [0, 0], 'k-', lw=10)
                        text1 = 'mean = %.2f' % meanb
                        plt.text(meanb, max(n), text1, ha='center', size='large')
                        plt.text(hdib[0], max(n) / 30, "%.2f" % hdib[0], ha='center', size='large')
                        plt.text(hdib[1], max(n) / 30, "%.2f" % hdib[1], ha='center', size='large')
                        plt.text(0.5 * (hdib[0] + hdib[1]),max(n) / 30, 'HDI', ha='center', size='large')
                        plt.pause(0.001)
                        plt.xlabel('b')

                        
                    plt.figure()
                    n, bins, pat = plt.hist(xfehs, bins=30, rwidth=0.90, color="lightsalmon")
                    plt.xlabel('[Fe/H]')
                    if mock_parameters!=0:
                        plt.plot([mfeh,mfeh], [0.,max(n)],'b--')
                    plt.plot([hdifeh[0], hdifeh[1]], [0, 0], 'k-', lw=10)
                    text2 = 'mean = %.2f' % meanfeh
                    plt.text(meanfeh, max(n), text2, ha='center', size='large')
                    plt.text(hdifeh[0], max(n) / 30, "%.2f" %hdifeh[0], ha='center', size='large')
                    plt.text(hdifeh[1], max(n) / 30, "%.2f" % hdifeh[1], ha='center', size='large')
                    plt.text(0.5 * (hdifeh[0] + hdifeh[1]), max(n) / 30, 'HDI', ha='center', size='large')
                    plt.pause(0.001)

                    plt.figure()
                    n, bins, pat = plt.hist(xfehs, bins=500, range=(self.lfehs[0], self.lfehs[self.nfehs - 1]),color="lightsalmon")
                    plt.xlabel('[Fe/H]')
                    if mock_parameters!=0:
                        plt.plot([mfeh,mfeh], [0.,max(n)],'b--')
                    for i in range(1, self.nfehs):
                        plt.plot([self.lfehs[i], self.lfehs[i]], [0., max(n)],':', color="lightgrey", lw=0)
                    plt.pause(0.001)

                    if self.itypef == "2":
                        # 2D plot (age, metallicity)
                        plt.figure()
                        plt.scatter(xages, xfehs, alpha=0.1, marker='.')
                        plt.plot(meanage, meanfeh, 'ro')
                        if self.itypemod == "4" or self.itypemod == "5":
                            plt.xlabel('Age(Gyr)')
                            if mock_parameters!=0:
                                plt.plot(ageyr,mfeh,'r*')   
                        if self.itypemod == "1" or self.itypemod == "3":
                            if mock_parameters!=0:
                                plt.plot(np.log10(ageyr),mfeh,'r*')   
                            plt.xlabel('log(Age(yr))')

                        plt.ylabel('[Fe/H]')
                        points = np.array([xages, xfehs]).T
                        plot_point_cov(points, nstd=2, fc="None", ec="red")
                        plt.pause(0.001)

                        plt.figure()
                        plt.hist2d(xages, xfehs, bins=30)
                        if self.itypemod == "4" or self.itypemod == "5":
                            plt.xlabel('Age(Gyr)')
                            if mock_parameters!=0:
                                plt.plot(ageyr,mfeh,'wo')
                        if self.itypemod == "1" or self.itypemod == "3":
                            plt.xlabel('Age(Gyr)')
                            if mock_parameters!=0:
                                plt.plot(np.log10(ageyr),mfeh,'wo')
                        plt.ylabel('[Fe/H]')
                        plt.pause(0.001)

                        plt.figure()
                        plt.hist2d(xages, xfehs, bins=500, range=[[self.lages[0], self.lages[self.nages - 1]],[self.lfehs[0], self.lfehs[self.nfehs - 1]]])
                        if self.itypemod == "4" or self.itypemod == "5":
                            plt.xlabel('Age(Gyr)')
                            if mock_parameters!=0:
                                 plt.plot(ageyr,mfeh,'wo')
                        if self.itypemod == "1" or self.itypemod == "3":
                            plt.xlabel('Age(Gyr)')
                            if mock_parameters!=0:
                                plt.plot(np.log10(ageyr),mfeh,'wo')
                        plt.ylabel('[Fe/H]')
                        for i in range(0, self.nages):
                            plt.plot([self.lages[i], self.lages[i]], [self.lfehs[0], self.lfehs[self.nfehs - 1]], ':', color="grey", lw=0.5)
                        for i in range(0, self.nfehs):
                            plt.plot([self.lages[0], self.lages[self.nages - 1]], [self.lfehs[i], self.lfehs[i]], ':', color="grey", lw=0.5)
                        plot_point_cov(points, nstd=2, fc="None", ec="red")
                        plt.pause(0.001)

                    if self.itypef == "4" or self.itypef == "6" or self.itypef =="8" or self.itypef == "9":
                        # 2D plot (mu, metallicity)
                        if self.itypef == "4" or self.itypef=="8":
                            xmus = (stanFit['mu'])
                        else:
                            xmus = (stanFit['t'])
                        meanmus = np.mean(xmus)
                        plt.figure()
                        plt.scatter(xmus, xfehs, alpha=0.1, marker='.')
                        plt.plot(meanmus, meanfeh, 'ro')
                        
                        if self.itypef == "4" or self.itypef=="8":
                            if mock_parameters!=0:
                                plt.plot(mu_mock,mfeh,'wo')
                            plt.xlabel('mu')
                        if self.itypef == "6" or self.itypef=="9":
                            if mock_parameters!=0:
                                plt.plot(t0yr,mfeh,'wo')
                            if self.itypemod== "1" or self.itypemod=="3":
                                plt.xlabel('t0(Gyr)')
                            if self.itypemod== "5" or self.itypemod=="6":
                                plt.xlabel('t0(Gyr)')
                        plt.ylabel('[Fe/H]')
                        points = np.array([xmus, xfehs]).T
                        plot_point_cov(points, nstd=2, fc="None", ec="red")
                        plt.figure()
                        plt.hist2d(xmus, xfehs, bins=30)
                        if self.itypef == "4" or self.itypef=="8":
                            plt.xlabel('mu')
                            if mock_parameters!=0:
                                plt.plot(mu_mock,mfeh,'r*')
                        if self.itypef == "6" or self.itypef=="9":
                            if mock_parameters!=0:
                                plt.plot(t0yr,mfeh,'r*')
                            if self.itypemod== "1" or self.itypemod=="3":
                                plt.xlabel('t0(Gyr)')
                            if self.itypemod== "5" or self.itypemod=="6":
                                plt.xlabel('t0(Gyr)')
                        plt.ylabel('[Fe/H]')
                        plt.figure()
                        if self.itypef == "4" or self.itypef=="8":
                            xa=stanFit["a"]
                            xb=stanFit["b"]
                            plt.hist2d(xa, xb, bins=30)
                            plt.xlabel('a')
                            plt.ylabel('b')
                            if mock_parameters!=0:
                                plt.plot(a,b,'r*')
                        if self.itypef == "6" or self.itypef=="9":
                            xt0yr=stanFit["t"]
                            xtauyr=stanFit["tau"]
                            plt.hist2d(xt0yr, xtauyr, bins=30)
                            if mock_parameters!=0:
                                plt.plot(t0yr,tauyr,'r*')
                            if self.itypemod== "1" or self.itypemod=="3":
                                plt.xlabel('t0(Gyr)')
                                plt.ylabel('tau0(Gyr)')
                            if self.itypemod== "5" or self.itypemod=="6":
                                plt.xlabel('t0(Gyr)')
                                plt.xlabel('tau(Gyr)')
                        
                        plt.figure()
                        plt.hist2d(xmus, xfehs, bins=500, range=[[0., 1.],[self.lfehs[0], self.lfehs[self.nfehs - 1]]])
                        if self.itypef == "4" or self.itypef=="8":
                            plt.xlabel('mu')
                            if mock_parameters!=0:
                                plt.plot(mu_mock,mfeh,'wo')
                        if self.itypef == "6" or self.itypef=="9":
                            if mock_parameters!=0:
                                plt.plot(t0yr,mfeh,'wo')
                                if mock_parameters!=0:
                                    plt.plot(mu_mock,mfeh,'wo')
                            if self.itypemod== "1" or self.itypemod=="3":
                                if mock_parameters!=0:
                                    plt.plot(t0yr,mfeh,'wo')
                                plt.xlabel('t0(Gyr)')
                            if self.itypemod== "5" or self.itypemod=="6":
                                plt.xlabel('t0(Gyr)')
                                if mock_parameters!=0:
                                    plt.plot(t0yr,mfeh,'wo')
                        plt.ylabel('[Fe/H]')
                        plot_point_cov(points, nstd=2, fc="None", ec="red")
                        plt.pause(0.001)

                if self.itypef == "3" or self.itypef == "4" or self.itypef =="8":
                    plt.ion()
                    plt.figure()
                    xa = stanFit['a']
                    xb = stanFit['b']
                    xran = np.arange(0.01, 1., 0.01)
                    xranp = xran * (self.x1 - self.x0) + self.x0
                    xages = stanFit['mu'] * (self.x1 - self.x0) + self.x0
                    hdisfh = np.zeros((self.nages, 2))
                    for i in range(self.nages):
                        sfh = beta.pdf(xages[i], a=xa, b=xb)
                        sfhmed = np.median(sfh)
                        plt.plot(self.lages[i], sfhmed, 'ko', markersize=2.)
                        hdisfh[i, ] = HDIofMCMC(sfh, credMass=0.66)
                        plt.plot([xages[i],xages[i]], hdisfh[i,], 'b-')

                    plt.plot(self.lages, hdisfh[:, 0], 'g-')
                    plt.plot(self.lages, hdisfh[:, 1], 'g-')

                    # input values. Cambiar aqui.
                    plt.figure()
                    xa2 =np.mean( stanFit['a'])
                    xb2 = np.mean( stanFit['b'])
                    plt.plot(xran,mibeta(xran,xa2,xb2),label="fit mean")
                    xa3 =np.median( stanFit['a'])
                    xb3 = np.median( stanFit['b'])
                    plt.plot(xran,mibeta(xran,xa3,xb3),label="fit median")
                    plt.plot(xran,mibeta(xran, a, b), 'r-',label="orginal")
                    plt.legend()
                    plt.pause(0.001)

                if self.itypef == "5" or self.itypef == "6" or self.itypef == "9":
                    plt.ion()
                    plt.figure()
                    if self.itypef=="5" or self.itypef =="6":
                        xt = stanFit['t'] * 1.E9
                        xtau = stanFit['tau'] * 1.E9
                        t0yr=t0yr*1.E9
                        tauyr=tauyr*1.E9
                    else:
                        xt = stanFit['t'] 
                        xtau = stanFit['tau'] 
                    meant = np.mean(xt)
                    meantau = np.mean(xtau)
                    mediant = np.median(xt)
                    mediantau = np.median(xtau)
                    

                    trange = np.arange(10**self.lages[self.nages - 1] * 1E-9, 0, -0.01)
                    xn = trange.size
                    sfhp = np.zeros(xn)
                    for i in range(xn):
                        if (trange[i] * 1.E9 > meant):
                            sfhp[i] = 0.
                        else:
                            telapsed = meant - trange[i] * 1E9
                            sfhp[i] = telapsed * np.exp(-telapsed / meantau)
                    sumsfhp = sum(sfhp)
                    sfhp = sfhp / sumsfhp * 10**self.lages[self.nages - 1] * 1E-9 * 100
                    plt.plot(trange, sfhp, 'b-' ,label="fit mean")
                    plt.xlabel('Age(Gyr)')
                    plt.ylabel('SFR')
                    # Now plot in green the median curve
                    for i in range(xn):
                        if (trange[i] * 1.E9 > mediant):
                            sfhp[i] = 0.
                        else:
                            telapsed = mediant - trange[i] * 1E9
                            sfhp[i] = telapsed * np.exp(-telapsed / mediantau)
                    sumsfhp = sum(sfhp)
                    sfhp = sfhp / sumsfhp * 10**self.lages[self.nages - 1] * 1E-9 * 100
                    plt.plot(trange, sfhp, 'g-' ,label="fit median")

                    # hdisfh = np.zeros((self.nages, 2))
                    # xage = np.zeros(self.nages)
                    # sfhmed = np.zeros(self.nages)
                    # nchain = chains * (niter - warmup)
                    # fage = np.zeros(nchain)

                    # for i in range(self.nages):
                    #     xage[i] = 10**self.lages[i]
                    #     for j in range(nchain):
                    #         if (xage[i] > xt[j]):
                    #             fage[j] = 0.
                    #         else:
                    #             telapsed = xt[j] - xage[i]
                    #             fage[j] = telapsed * np.exp(-telapsed / xtau[j])
                    #     fage = fage / sumsfhp * 1890.

                    #     hdisfh[i, ] = HDIofMCMC(fage, credMass=0.95)
                    #     sfhmed[i] = np.mean(fage)
                    # plt.plot(xage * 1E-9, sfhmed, 'ko', markersize=2.)
                    #            plt.plot(xage*1E-9, hdisfh[:,0],'g-')
                    #            plt.plot(xage*1E-9, hdisfh[:,1],'g-')
                    #now print the original curve
                    if mock_parameters!=0:
                        for i in range(xn):
                            if (trange[i] * 1.E9 > t0yr):
                                sfhp[i] = 0.
                            else:
                                telapsed = t0yr - trange[i] * 1E9
                                sfhp[i] = telapsed * np.exp(-telapsed / (tauyr))
                        sumsfhp = sum(sfhp)
                        sfhp = sfhp / sumsfhp * 10**self.lages[self.nages - 1] * 1E-9 * 100
                        plt.plot(trange, sfhp, 'r-',label="mock")
                        plt.legend()
                        #            plt.plot(xage*1E-9, hdisfh[:,0],'g-')
                        #            plt.plot(xage*1E-9, hdisfh[:,1],'g-')
                            
                        plt.pause(0.001)
                            
                    
        # Grafica logaritmica
                    plt.figure()
                    trange = np.arange(self.lages[0], self.lages[self.nages - 1], 0.02)
                    atrange = 10**trange
                    xn = trange.size
                    sfhp = np.zeros(xn)
                    for i in range(xn):
                        if (atrange[i] > meant):
                            sfhp[i] = 0.
                        else:
                            telapsed = meant - atrange[i]
                            sfhp[i] = telapsed * np.exp(-telapsed / meantau)
                    sumsfhp = sum(sfhp)
                    sfhp = sfhp / sumsfhp * 200
                    plt.plot(trange, sfhp, 'b-',label="fit mean")
                    plt.xlabel('Age(Gyr)')
                    plt.ylabel('SFR')
                    for i in range(xn):
                        if (atrange[i] > mediant):
                            sfhp[i] = 0.
                        else:
                            telapsed = mediant - atrange[i]
                            sfhp[i] = telapsed * np.exp(-telapsed / mediantau)
                    sumsfhp = sum(sfhp)
                    sfhp = sfhp / sumsfhp * 200
                    plt.plot(trange, sfhp, 'g-',label="fit median")
                    plt.pause(0.001)
                    if mock_parameters!=0:
                        for i in range(xn):
                            if (atrange[i] > t0yr):
                                sfhp[i] = 0.
                            else:
                                telapsed = t0yr - atrange[i]
                                sfhp[i] = telapsed * np.exp(-telapsed / (tauyr))
                        sumsfhp = sum(sfhp)
                        sfhp = sfhp / sumsfhp * 200
                        plt.plot(trange, sfhp, 'r-',label="mock")
                        plt.pause(0.001)
                        plt.legend()
                plt.pause(0.001)

                plt.show()

                
            if action == 'T':
                la = stanFit.extract(permuted=False, inc_warmup=True)

                icoef = 0
                if self.itypef == "1" or self.itypef == "2":
                    ymaxt1 = 50.
                    ymaxt2 = 7.
                if self.itypef == "3" or self.itypef == "5":
                    ymaxt1 = 1.
                    ymaxt2 = 100.
                plt.ion()
                plt.figure()
                for i in range(chains):
                    plt.plot(la[:, i, icoef], linewidth=1.0)
        #        plt.plot([warmup,warmup],[0.,ymaxt1],'k-')
                plt.text(warmup / 2, 0., 'warmup', horizontalalignment='center')
        #        plt.draw()
                plt.pause(0.001)

                icoef = 1
                plt.figure()
                for i in range(chains):
                    plt.plot(la[:, i, icoef], linewidth=1.0)
        #        plt.plot([warmup,warmup],[0.,ymaxt2],'k-')
                plt.text(warmup / 2, 0., 'warmup', horizontalalignment='center')
        #        plt.draw()
                plt.pause(0.001)
                plt.show()

            if action == 'W':
                # Calculo de WAIC

                # Calculo de los espectros predichos para cada paso de la cadena

                nchain = chains * (niter - warmup)
                ypred = np.zeros((nchain, self.infilt))
                error = stanFit['erry']

                if self.itypef == "1" or self.itypef == "2":

                    for k in range(nchain):
                        agek = stanFit['age'][k] - 1.
                        metk = 0
                        if self.itypef == "2":
                            metk = stanFit['feh'][k] - 1.
                        ypred[k, ] = mcsp.predmodel12(self.itypef, self.infilt, agek, metk, self.nages, self.nfehs, self.modelsb)

                if self.itypef == "3" or self.itypef == "4":
                    self.x0 = self.minage - self.stepage / 2.
                    self.x1 = self.minage + self.stepage * self.nages - self.stepage / 2.
                    xages = (self.lages - self.x0) / (self.x1 - self.x0)
                    incage = np.zeros(self.nages)
                for i in range(self.nages):
                    incage[i] = (10.**(self.lages[i] + self.stepage / 2) - 10. ** (self.lages[i] - self.stepage / 2)) / 1.e9
                    for k in range(nchain):
                        xa = stanFit['a'][k]
                        xb = stanFit['b'][k]
                        metk = 0
                        if self.itypef == "4":
                            metk = stanFit['feh'][k] - 1.
                        ypred[k, ] = mcsp.predmodel34(self.itypef, self.infilt, xa, xb, metk, self.nages,
                                                      self.nfehs, self.modelsb, xages, incage)

                if self.itypef == "5" or self.itypef == "6":
                    limage = np.zeros(self.nages + 1)
                    limage[0] = 10**(self.lages[0] - self.stepage / 2.)
                    for i in range(self.nages):
                        limage[i + 1] = 10**(self.lages[i] + self.stepage / 2)

                    for k in range(nchain):
                        xt = stanFit['t'][k] * 1.E9
                        xtau = stanFit['tau'][k] * 1.E9
                        metk = 0
                        if self.itypef == "6":
                            metk = stanFit['feh'][k] - 1.
                        ypred[k, ] = mcsp.predmodel56(self.itypef, self.infilt, xt, xtau, metk, self.nages, self.nfehs, self.modelsb, limage)

        # Ahora calcula waic

                pr = np.zeros(self.infilt)
                v = np.zeros(self.infilt)
                for i in range(self.infilt):
                    print(i, self.spobs[i], ypred[0, i])
                    vero = norm.pdf(self.spobs[i], loc=ypred[:, i], scale=error[:])
                    pr[i] = np.mean(vero)
                    v[i] = np.var(np.log(vero))
                lppd = np.sum(np.log(pr))
                pwaic = np.sum(v)
                WAIC = -2. * (lppd - pwaic)
                print("WAIC : ", WAIC, "    pwaic : ", pwaic)
                waici = 2. * (np.log(pr) - pwaic)
                EWAIC = np.sqrt(self.infilt * np.var(waici))
                print("EWAIC : ", EWAIC)

            if action == 'X':
                plt.ion()
                plt.figure()
                ypred=0
                if self.itypemod == "1" or self.itypemod == "3" or self.itypemod == "4"or self.itypemod == "5":
                    plt.plot(self.spobs, 'k')
                elif self.itypemod == "2":
                    #plt.plot(self.spobs,'ok')
                    plt.errorbar(ldo, self.spobs, yerr=self.spobse, fmt='ok')
                if self.itypef == "1" or self.itypef == "2" or self.itypef=="7":
                    agek = np.mean(stanFit['age']) - 1.
                    metk = 0.
                    if self.itypef == "2":
                        metk = np.mean(stanFit['feh']) - 1.
                    ypred = mcsp.predmodel12(self.itypef, self.infilt, agek, metk, self.nages, self.nfehs, self.modelsb)

                if self.itypef == "3" or self.itypef == "4" or self.itypef == "8":
                    if self.itypemod == "1"or self.itypemod == "3":
                        self.x0 = self.minage - self.stepage / 2.
                        self.x1 = self.minage + self.stepage * self.nages - self.stepage / 2.
                        xages = (self.lages - self.x0) / (self.x1 - self.x0)
                        incage = np.zeros(self.nages)
                        for i in range(self.nages):
                            incage[i] = (10.**(self.lages[i] + self.stepage / 2) - 10. **(self.lages[i] - self.stepage / 2)) / 1.e9
                    elif self.itypemod == "4"or self.itypemod == "5":
                       self.x0 = self.lages[0] - (self.lages[1] - self.lages[0]) / 2.
                       self.x1 = self.lages[self.nages - 1] + (self.lages[self.nages - 1] - self.lages[self.nages - 2]) / 2.
                       self.lages = np.asarray(self.lages)
                       xages = (self.lages - self.x0) / (self.x1 - self.x0)
                       incage = np.zeros(self.nages)
                       for i in range(self.nages):
                           if i == self.nages - 1:
                               incage[i] = self.lages[i] - self.lages[i - 1]
                           else:
                               incage[i] = self.lages[i + 1] - self.lages[i]
                    xa = np.mean(stanFit['a'])
                    xb = np.mean(stanFit['b'])
                    if self.itypef == "4" or self.itypef== "8":
                        metk = np.mean(stanFit['feh']) - 1.
                        if self.itypef == "8":
                            obs=np.zeros([self.nages*self.nfehs,self.infilt])
                            mrv=np.mean(stanFit['Rv']) - 1.
                            i1 = int(np.floor(mrv))
                            i2 = i1 +1
                            weight1 = i2 - mrv
                            weight2 = 1. - weight1
                            for j in range(self.infilt):
                                for k in range(self.nfehs*self.nages):
                                    obs[k,j] = self.modelsb[k+i1*self.nages*self.nfehs,j]*weight1 + self.modelsb[k+i2*self.nages*self.nfehs,j]*weight2      
                            ypred = mcsp.predmodel34(self.itypef, self.infilt, xa, xb, metk, self.nages, self.nfehs, obs, xages, incage)
                        else:
                            ypred = mcsp.predmodel34(self.itypef, self.infilt, xa, xb, metk, self.nages, self.nfehs, self.modelsb, xages, incage)
                        
                if self.itypef == "5" or self.itypef == "6":
                    limage = np.zeros(self.nages + 1)
                    limage[0] = 10**(self.lages[0] - self.stepage / 2.)
                    for i in range(self.nages):
                        limage[i + 1] = 10**(self.lages[i] + self.stepage / 2)
                    xt = np.mean(stanFit['t']) * 1.E9
                    xtau = np.mean(stanFit['tau']) * 1.E9
                    metk = 0.
                    if self.itypef == "6":
                        metk = np.mean(stanFit['feh']) - 1.
                    ypred = mcsp.predmodel56(self.itypef, self.infilt, xt, xtau, metk, self.nages, self.nfehs, self.modelsb, limage)
                    
                if self.itypef != "9":
                    plt.plot(ypred, 'r')
                    plt.figure()
                    print(a)
                    order = np.argsort(self.slambdab)
                    x_order = np.array(self.slambdab)[order]
                    y_order = np.array(self.spobs)[order]
                    #y2_order=np.array(ypred)[order]
                    #plt.plot(self.slambdab,ypred,label="fit")
                    #plt.plot(self.slambdab,self.spobs,'k',label="mock")
                    #plt.plot(x_order,y2_order,label="fit")
                    plt.rc('text', usetex=True)
                    plt.plot(x_order,y_order,'k',label="mock")
                    #plt.plot(x_order,y2_order,label="fit")
                    #plt.errorbar(x_order,y2_order,xerr=self.slambdab_error,label="fit")
                    plt.legend()
                    plt.xlabel(r'Wavelength(\AA)')
                elif self.itypemod == "2":
                    plt.plot(ldo, ypred, 'r')
                if self.cerrob =="Y":
                    #plt.errorbar(self.slambdab,ypred, yerr=self.spobse)
                    plt.figure()
                    plt.plot(self.slambdab,stanFit["yreal"][0],'k',label="Fit")
                    plt.errorbar(self.slambdab,self.spobs, yerr=self.spobse,label="Challenge")
                    plt.legend()
                    plt.figure()
                    order = np.argsort(self.slambdab)
                    x_order = np.array(self.slambdab)[order]
                    y_order = np.array(self.spobs)[order]
                    y2_order=np.array(stanFit["yreal"][0])[order]
                    #plt.plot(self.slambdab,ypred,label="fit")
                    #plt.plot(self.slambdab,self.spobs,'k',label="mock")
                    #plt.plot(x_order,y2_order,label="fit")
                    plt.rc('text', usetex=True)
                    plt.plot(x_order,y_order,'k',label="mock")
                    plt.plot(x_order,y2_order,label="fit")
                    #plt.errorbar(x_order,y2_order,xerr=self.slambdab_error,label="fit")
                    plt.legend()
                    plt.xlabel(r'Wavelength(\AA)')
                    plt.figure()
                    order = np.argsort(self.slambdab)
                    x_order = np.array(self.slambdab)[order]
                    y_order = np.array(self.spobs)[order]
                    y2_order=np.array(stanFit["yreal"][0])[order]
                    #plt.plot(self.slambdab,ypred,label="fit")
                    #plt.plot(self.slambdab,self.spobs,'k',label="mock")
                    #plt.plot(x_order,y2_order,label="fit")
                    plt.rc('text', usetex=True)
                    plt.errorbar(x_order,y_order,yerr=self.spobse,label="mock")
                    plt.plot(x_order,y2_order,'k',label="fit")
                    #plt.errorbar(x_order,y2_order,xerr=self.slambdab_error,label="fit")
                    plt.legend()
                    plt.xlabel(r'Wavelength(\AA)')
                
                #self.resolve_fv=ypred
                self.resolve_parameters=stanFit
                plt.pause(0.001)
                plt.show()

        # stops here

        #         Hay un error, al crear el especto con mcmcsp_mock y unos parámetros determinados
        #         crea un espectro muy diferente del que se crea aqui con los mismos parámetros

