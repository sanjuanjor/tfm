from __future__ import division
from __future__ import print_function
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
import numpy as np
import matplotlib.pyplot as plt
import pyphot



name_of_file="Challenge-master/filters.dat"
lib = pyphot.get_library()
list = []
x = np.genfromtxt(name_of_file, dtype=None)
for i in x:
    # print(i[1])
    f = lib.find(i[1])
    list.append(f)
print(list)
i=0
for w in list:
    i+=1
    f = lib[w[0]]
    wavelength=(f._wavelength)
    transmit=(f.transmit)
    plt.subplot(5, 3, i//5+1)
    plt.plot(wavelength,transmit,label=x[i-1][1])#f.name)
    plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, mode="expand", borderaxespad=0.)
    #plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
plt.show()


