from __future__ import division 
from __future__ import print_function
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.stats import norm, beta
import pickle
from pystan import StanModel
from astropy.io import fits
from fjg import *
import mcmcsp_fun as mcsp
import pyphot
from graphics import spectrums

itypemod = "1"
itypef = "5"
cerrob= "N"
print("Individual error bars",cerrob)
bin = 10
print("Bin =",bin)
emiles = spectrums()
emiles.itypef = itypef
emiles.itypemod = itypemod
emiles.cerrob = cerrob
emiles.load_spectra(bin,0)
ageyr=1.5
mfeh= -0.9
a=5.0
b=20.0
t0yr=10E9
tauyr=1E9
error=0.02
parameters = {'ageyr': ageyr, 'mfeh': mfeh,'a': a, 'b': b, 't0yr': t0yr,'tauyr':tauyr,"error":error}
emiles.create_model("test.ref",parameters)
emiles.load_observation("test.ref")
chains = 4  # 3
warmup =3000  # 500
niter = 9500  # 9500
thin = 1
n_jobs =4
stanparameters = {'chains': chains, 'warmup': warmup,'niter': niter, 'thin': thin, 'n_jobs': n_jobs}
#emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the library
#normal just testing
data_stan = emiles.data_stan()
dataforstan = data_stan["1"]
modelfile = data_stan["2"]
fitfile = data_stan["3"]
emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,parameters,"0")
# name_of_file="Challenge-master/filters.dat"

# #plt.figure()
# plt.plot(emiles.slambdab,emiles.spobs,label="Spectrum")
# #emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the library
# plt.plot(emiles.slambdab,emiles.spobs,"o",label="Photometry")
# plt.rc('text', usetex=True)
# plt.xlabel(r'Wavelength(\AA)')

# plt.figure()
# name_of_file="Challenge-master/filters.dat"
# lib = pyphot.get_library()
# list = []
# x = np.genfromtxt(name_of_file, dtype=None)
# for i in x:
#     # print(i[1])
#     f = lib.find(i[1])
#     list.append(f)
# print(list)
# i=0
# for w in list:
#     i+=1
#     f = lib[w[0]]
#     wavelength=(f._wavelength)
#     transmit=(f.transmit)
#     plt.plot(wavelength,transmit,label=x[i-1][1])#f.name)
# summodel = sum(emiles.spobs)
# emiles.spobs = emiles.spobs/4
# plt.plot(emiles.slambdab,emiles.spobs)
# plt.xlabel(r'Wavelength(\AA)')
# plt.show()              
# plt.pause(60)
