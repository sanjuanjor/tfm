data {
  int<lower=0> npix ;
  int<lower=0> nages ;
  vector[npix] yobs ;
  matrix[nages,npix] ymod ;
  vector[nages] xages ;
  vector[nages] incage ;
}
parameters {
  real<lower=0.,upper=1.> mu ;
  real<lower=0.> theta ;
  real<lower=0.> erry ;
}
transformed parameters {
  real<lower=0.> a ;
  real<lower=0.> b ;
  a = theta * mu ;
  b = theta * (1. - mu) ;
}
model{
  real sfh ;
  real suma ;
  vector[npix] yinterp ;

  erry ~ normal(0.,1.) ;
  theta ~ normal(0.,50.) ;
  for (j in 1:npix) {
    yinterp[j] = 0. ;
  }
  for (i in 1:nages) {
      sfh = pow(xages[i], a-1.) * pow(1.-xages[i], b-1.) ;
      yinterp = yinterp + (ymod[i])' * sfh * incage[i] ;
  }
  suma = sum(yinterp) ;
  yinterp = yinterp/suma*npix ;
  yobs ~ normal(yinterp, erry) ;
}
