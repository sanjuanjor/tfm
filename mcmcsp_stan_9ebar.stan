data {
  int<lower=0> npix ;
  int<lower=0> nages ;
  int<lower=0> nfehs ;
  int<lower=0> nRv;
  vector[npix] yobs ;
  vector[npix] yerr ;
  matrix[nages*nfehs*nRv,npix] ymod ;
  vector[nages+1] limage ;
  real tmin ;
  real tmax ;
}
parameters {
  real<lower=tmin,upper=tmax> t ;
  real<lower=0.> tau ;
  real<lower=1.,upper=nfehs> feh ;
  real<lower=1.,upper=nRv> Rv ;	
  real<lower=0.> erry ;
  vector[npix] yreal ;
}
model{
  int iz2 ;
  int iRv2;	
  int j1 ;
  int j2 ;
  int j3 ;
  int j4 ;
  real weigz1 ;
  real weigz2 ;
  real weigRv1;
  real weigRv2 ;	
  real suma ;
  real ta ;
  real tb ;
  vector[nages] wei ;
  vector[npix] yinterp ;
  vector[npix] yinterp1 ;
  vector[npix] yinterp2 ;
  vector[npix] yinterp3 ;
  vector[npix] yinterp4 ;
  vector[npix] yinterp5 ;
  vector[npix] yinterp6 ;
  
  erry ~ normal(0.,1.) ;
  tau ~ normal(0.,15.) ;

  iz2 = 1 ;
  while (iz2 < feh)
    iz2 = iz2 + 1 ;
  weigz1 = iz2 - feh ;
  weigz2 = 1. - weigz1 ;

  iRv2 = 1 ;
  while (iRv2 < Rv)
    iRv2 = iRv2 + 1 ;
  weigRv1 = iRv2 - Rv ;
  weigRv2 = 1. - weigRv1 ;
  
  for (i in 1:nages) {
      ta = fmax(0., t-limage[i+1]) ;
      tb = fmax(0., t-limage[i]) ;
      wei[i] = tau*((ta+tau)*exp(-ta/tau) - (tb+tau)*exp(-tb/tau)) ;
  }

  for (j in 1:npix) {
    yinterp1[j] = 0. ;
    yinterp2[j] = 0. ;
    yinterp3[j] = 0. ;
    yinterp4[j] = 0. ;
  }


  j1 = (iz2 - 2) * nages+ (iRv2-2)*nfehs*nages  ;
  j2 = (iz2 - 2) * nages+ (iRv2-1)*nfehs*nages  ;
  j3= (iz2 - 1) * nages+ (iRv2-2)*nfehs*nages  ;
  j4= (iz2 - 1) * nages+ (iRv2-1)*nfehs*nages  ;
  for (i in 1:nages) {
      yinterp1 = yinterp1 + (ymod[j1+i])' * wei[i] ;
      yinterp2 = yinterp2 + (ymod[j2+i])' * wei[i] ;
      yinterp3 = yinterp3 + (ymod[j3+i])' * wei[i] ;
      yinterp4 = yinterp4 + (ymod[j4+i])' * wei[i] ;
  }
  yinterp5 = yinterp1*weigRv1 + yinterp2* weigRv2;
  yinterp6 =  yinterp3*weigRv1 + yinterp4* weigRv2;


  yinterp = yinterp5*weigz1 + yinterp6*weigz2 ;

  suma = sum(yinterp) ;
  yinterp = yinterp/suma*npix ;
  
  yreal ~ normal(yinterp, erry) ;
  yobs ~ normal(yreal, yerr) ;
}
