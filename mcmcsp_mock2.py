# -*- coding: utf-8 -*-
# mcmcsp_1.py
# v 2 (single burst, age + metallicity)
# @ Javier Gorgas

from __future__ import division
from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
import pickle

from astropy.io import fits

def mibeta(x, a, b) :
    y = x**(a-1.) * (1.-x)**(b-1.)
    sy = sum(y)
    y = y/sy
    return y

# rango de edades (en log(years) de los modelos de MILES
minage = 7.80
maxage = 10.26
stepage = 0.05
lages = np.arange(minage, maxage, stepage)
nages = lages.size

# rango de metalicidades
lfehs = [-2.32, -1.71, -1.31, -0.71, -0.40, 0, 0.22]
nfehs = len(lfehs)

npixels = 4300

print('Type of mock galaxy:')
print('    1 = single burst (only age, solar Z)')
print('    2 = single burst (age and metallicity)')
print('    3 = beta SFH (solar Z)')
print('    4 = beta SFH (variable Z)')
print('    5 = delayed exponential SFH (solar Z)')
print('    6 = delayed exponential SFH (variable Z)')
itypef = raw_input('? ')
#itypef = "2"

if itypef == "1" or itypef == "3" or itypef == "5" :
    models = np.zeros((nages,npixels))
    nfehs = 1
    lfehs = [0]
    
elif itypef == "2" or itypef == "4" or itypef == "6" :
    models = np.zeros((nages*nfehs,npixels))

else :
    print('ERROR: Not implemented')
        
# lectura de los modelos originales
# numero de pixels de los modelos

dire = "MILES_SSPs/MILES_Padova00_un_1.30_fits/"

k = 0
for m in range(nfehs) : 
    chmet = '%+5.2f' % lfehs[m]
    chmet = 'm' + chmet[1:] if lfehs[m] < 0 else 'p' + chmet[1:]
    print(m,chmet)
    for i in range(nages):
        chage = '%07.4f' % (10**lages[i]*1E-9)
        filename = dire + "Mun1.30Z" + chmet + "T" + chage + "_iPp0.00_baseFe.fits"
        hdul = fits.open(filename)
        models[k,] = hdul[0].data
        hdul.close()

        k += 1
    
# Hacemos un binning (o no)
bin = 10
bin = int(raw_input('Bin? (0=none) '))
if bin > 1 : 
    infilt  = int(npixels/bin)
    modelsb = np.zeros((nages*nfehs,infilt))
    for i in range(nages*nfehs):
        for j in range(infilt):
            j1 = j*bin
            j2 = j1 + bin
            modelsb[i,j] = np.mean(models[i,j1:j2])
        # plt.plot(modelsb[i,])
else:
    infilt  = npixels
    modelsb = models
        
# Mock galaxy

modelinter = np.zeros((nfehs,infilt))
modelobs = np.zeros(infilt)
if itypef == "1" or itypef == "2" :

  ageyr = float(raw_input('Mock galaxy age (in years) (0.0631E9-17.7828E9)? '))
  #ageyr = 1.E9
  if (ageyr < 0.0631E9 or ageyr > 17.7828E9) :
    print('ERROR: age out of range')
  lageyr = (np.log10(ageyr) - minage)/stepage

  if itypef == "2" :
    mfeh = float(raw_input('Metallicity (in [Fe/H]) (-2.32 - +0.22)? '))
#    mfeh = 0.
    if (mfeh < -2.32 or mfeh > 0.22) :
        print('ERROR: metallicity out of range')

  i1 = int(np.floor(lageyr))
  i2 = i1 +1
  weight1 = i2 - lageyr
  weight2 = 1. - weight1

  for m in range(nfehs) : 
    for j in range(infilt):
            i1m = i1 + nages * m
            i2m = i1m + 1
            modelinter[m,j] = modelsb[i1m,j]*weight1 + modelsb[i2m,j]*weight2

  if itypef == "1" :
    modelobs = modelinter[0,]
  else :
    jj = 0
    while mfeh > lfehs[jj+1] :
            jj += 1
    i1 = jj
    i2 = i1 +1
    weight1 = (lfehs[i2] - mfeh)/(lfehs[i2] - lfehs[i1])
    weight2 = 1. - weight1
    for j in range(infilt):
        modelobs[j] = modelinter[i1,j]*weight1 + modelinter[i2,j]*weight2

if itypef == "3" or itypef == "4":
    a = float(raw_input('Enter "a" parameter? '))
    b = float(raw_input('Enter "b" parameter? '))
    x0 = minage-stepage/2.
    x1 = minage+stepage*nages-stepage/2.
    xages = (lages-x0)/(x1-x0)
    sfh = mibeta(xages, a, b)
    plt.ion() 
    plt.figure()
    plt.plot(lages, sfh, 'b-')
    plt.plot(lages, sfh, 'k.')
    plt.pause(0.001)
    plt.show()
    
    incage = np.zeros(nages)
    for i in range(nages):
        incage[i] = (10.**(lages[i]+stepage/2)-10.**(lages[i]-stepage/2))/1.e9

    if itypef == "4" :
        mfeh = float(raw_input('Metallicity (in [Fe/H]) (-2.32 - +0.22)? '))
        if (mfeh < -2.32 or mfeh > 0.22) :
            print('ERROR: metallicity out of range')

    for m in range(nfehs) : 
        for i in range(nages):
            for j in range(infilt):
                im = i + nages*m
                modelinter[m,j] = modelinter[m,j] + modelsb[im,j]*sfh[i]*incage[i]

    if itypef == "3" :
        modelobs = modelinter[0,]
    else :
        jj = 0
        while mfeh > lfehs[jj+1] :
                jj += 1
        i1 = jj
        i2 = i1 +1
        weight1 = (lfehs[i2] - mfeh)/(lfehs[i2] - lfehs[i1])
        weight2 = 1. - weight1
        for j in range(infilt):
            modelobs[j] = modelinter[i1,j]*weight1 + modelinter[i2,j]*weight2

if itypef == "5" or itypef == "6":

  t0yr = float(raw_input('t parameter (in years) (0.06E9-18.9E9)? '))
  if (t0yr < 0.06E9 or t0yr > 18.9E9) :
    print('ERROR: t out of range')
  tauyr = float(raw_input('tau parameter (in years) (>0)? '))
  if (tauyr < 0.0) :
    print('ERROR: tau out of range')
  if itypef == "6" :
    mfeh = float(raw_input('Metallicity (in [Fe/H]) (-2.32 - +0.22)? '))
    if (mfeh < -2.32 or mfeh > 0.22) :
        print('ERROR: metallicity out of range')
    
  trange = np.arange(18.9, 0, -0.01)
  xn = trange.size
  sfhp = np.zeros(xn)  
  for i in range(xn) :
     if (trange[i] * 1.E9 > t0yr) :
        sfhp[i] = 0.
     else :
        telapsed = t0yr - trange[i] * 1E9
        sfhp[i] = telapsed * np.exp(-telapsed/tauyr)
  sumsfhp = sum(sfhp)     
  sfhp = sfhp/sumsfhp*1890.
  plt.ion() 
  plt.figure()
  plt.plot(trange, sfhp, 'b-')
  
  xage = np.zeros(nages) 
  limage = np.zeros(nages+1)
  limage[0]=10**7.775
  wei = np.zeros(nages) 
  fage = np.zeros(nages) 
  for i in range(nages) :
     xage[i] = 10**lages[i]
     limage[i+1] =  10**(lages[i]+stepage/2)
     if (xage[i] > t0yr) :
        fage[i] = 0.
     else :
        telapsed = t0yr - xage[i]
        fage[i] = telapsed * np.exp(-telapsed/tauyr)
        
     ta = max(0., t0yr-limage[i+1])
     tb = max(0., t0yr-limage[i])
     wei[i] = tauyr*((ta+tauyr)*np.exp(-ta/tauyr) - (tb+tauyr)*np.exp(-tb/tauyr))  
        
#     plt.plot([limage[i+1]*1E-9,limage[i+1]*1E-9],[0.,10.],'--')
  weit = sum(wei)
  wei = wei/weit
  fage = fage/sumsfhp*1890.
  plt.plot(xage*1E-9, fage, 'k.')
  plt.pause(0.001)
  plt.show()
  
  for m in range(nfehs) : 
    for i in range(nages):
        for j in range(infilt):
            im = i + nages*m
            modelinter[m,j] = modelinter[m,j] + modelsb[im,j]*wei[i]

  if itypef == "5" :
        modelobs = modelinter[0,]
  else :
        jj = 0
        while mfeh > lfehs[jj+1] :
                jj += 1
        i1 = jj
        i2 = i1 +1
        weight1 = (lfehs[i2] - mfeh)/(lfehs[i2] - lfehs[i1])
        weight2 = 1. - weight1
        for j in range(infilt):
            modelobs[j] = modelinter[i1,j]*weight1 + modelinter[i2,j]*weight2

# se normaliza
summodel = sum(modelobs)
modelobs = modelobs/summodel*infilt

error = float(raw_input('Error (0-1)? '))
#error = 0.1
spobs = norm.rvs(modelobs, error, size=infilt)
plt.ion() 
plt.figure() 
plt.plot(spobs,'k')
plt.pause(0.001)
plt.show() 
 
# Graba el espectro
outfile = raw_input('Output spectrum file name? ')
F = open(outfile,'wb')
pickle.dump(spobs, F)
F.close() 

# stops here
                     