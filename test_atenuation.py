from __future__ import division 
from __future__ import print_function
import warnings
warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from scipy.stats import norm, beta
import pickle
from pystan import StanModel
from astropy.io import fits
from fjg import *
import mcmcsp_fun as mcsp
import pyphot
from graphics import spectrums

itypemod = "4"
itypef = "8"
cerrob = "Y"
bin = 10
redshift=0.21

emiles = spectrums()
emiles.itypef = itypef
emiles.itypemod = itypemod
emiles.cerrob = cerrob
#emiles.Rv=[2.0,2.1,2.2,2.3,2.4,2.5,2.6,2.7,2.8,2.9,3.,3.1,3.2,3.3,3.4,3.5,3.6,3.7,3.8,3.9,4.0,4.1,4.2,4.3,4.4,4.5,4.6,4.7,4.8,4.9,5.0]
emiles.Rv=[.5,1.0,1.5,2.0,2.5,3.,3.5,4.0,4.5,5.0]
emiles.load_spectra(bin,redshift)

ageyr=1E9
mfeh= 0.2
a=5.0
b=20.0
t0yr=5E9
tauyr=0.5E9
error=0.02
Rv=3.1

parameters = {'ageyr': ageyr, 'mfeh': mfeh,'a': a, 'b': b, 't0yr': t0yr,'tauyr':tauyr,"error":error,"Rv":Rv}
emiles.create_model("d.ref",parameters)
emiles.load_observation("d.ref")
chains = 4  # 3
warmup = 1500  # 500
niter = 9500  # 9500
thin = 1
n_jobs = 4
stanparameters = {'chains': chains, 'warmup': warmup,'niter': niter, 'thin': thin, 'n_jobs': n_jobs}

emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the librari
f=open("Challenge-master/total_file_z021.dat",'r')
num_lines = sum(1 for line in f)
f.close()
f=open("Challenge-master/total_file_z021.dat",'r')


N=int(num_lines/2)
erry=np.zeros(N)
erryrhat=np.zeros(N)
meanfeh=np.zeros(N)
meanage=np.zeros(N)
sdfeh=np.zeros(N)
sdage=np.zeros(N)
hdifeh=np.zeros(N)
hdiage=np.zeros(N)
agerhat=np.zeros(N)
fehrhat=np.zeros(N)
meant=np.zeros(N)
meantau=np.zeros(N)
taurhat=np.zeros(N)
meantheta=np.zeros(N)
meanmus=np.zeros(N)
musrhat=np.zeros(N)
trhat=np.zeros(N)
thetarhat=np.zeros(N)
xa=np.zeros(N)
xb=np.zeros(N)
Rv=np.zeros(N)
RvRhat=np.zeros(N)
sdrv=np.zeros(N)
Results=np.zeros((N,emiles.infilt))
#compile
data_stan = emiles.data_stan()
dataforstan = data_stan["1"]
modelfile = data_stan["2"]
fitfile = data_stan["3"]
emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,0,'C')

for i in range(5):#N):
    emiles.spobs=np.fromstring(f.readline(),dtype=float,sep=' ')
    emiles.spobse=np.fromstring(f.readline(),dtype=float,sep=' ')
    summodel = sum(emiles.spobs)
    emiles.spobs= emiles.spobs / summodel * emiles.infilt
    emiles.spobse= emiles.spobse / summodel * emiles.infilt
    data_stan = emiles.data_stan()
    dataforstan = data_stan["1"]
    modelfile = data_stan["2"]
    fitfile = data_stan["3"]
    T=True
    count=0
    while T:
        count+=8
        emiles.run_stan(dataforstan, modelfile, fitfile, stanparameters,0,'R')
        F = open(fitfile, 'rb')
        stanFit = pickle.load(F)
        F.close()
        print(stanFit.stansummary())
        output = str(stanFit).split('\n')
        nlines = 11 + emiles.nparameters + 2
        for item in output[1:nlines]:
            print(item)
        irhat = stanFit.summary()['summary_colnames']
        irhat = irhat.index("Rhat")
        irhat = stanFit.summary()["summary"][:, irhat]
        if (irhat[0] and irhat[1] < 1.1) or count > 10:
            T=False
        else:
            T = True

    Results[i,]=stanFit["yreal"][0]
    
    print(i/N,"%")
    xfehs = np.zeros(len(stanFit['feh']))
    for j in range(len(stanFit['feh'])):
       xf = stanFit['feh'][j]
       bf = int(xf) - 1
       xfehs[j] = emiles.lfehs[bf] + (xf - 1. - bf) * (emiles.lfehs[bf + 1] - emiles.lfehs[bf])
    meanfeh[i] = np.mean(xfehs)
    sdfeh[i] = np.std(xfehs, ddof=1)
    fehrhat[i]=irhat[2]
    
    #hdifeh[i] = HDIofMCMC(xfehs, credMass=0.95)
    if emiles.itypef == "3" or emiles.itypef == "4" or emiles.itypef == "8":
        xxages = stanFit['mu'] * (emiles.x1 - emiles.x0) + emiles.x0
        meanage[i] = np.mean(xxages)
        sdage[i] = np.std(xxages, ddof=1)
        hdiage = HDIofMCMC(xxages, credMass=0.95)
        xmus = (stanFit['mu'])
        meanmus[i] = np.mean(xmus)
        musrhat[i] = irhat[0]
        xtheta = (stanFit['theta'])
        meantheta[i] = np.mean(xtheta)
        thetarhat[i] = irhat[1]
        xa[i] = np.mean(stanFit['a'])
        xb[i] = np.mean(stanFit['b'])
    if emiles.itypef == "9":
        #xxages = stanFit['mu'] * (emiles.x1 - emiles.x0) + emiles.x0
        #meanage[i] = np.mean(xxages)
        #sdage[i] = np.std(xxages, ddof=1)
        #hdiage = HDIofMCMC(xxages, credMass=0.95)
        xt = (stanFit['t'])
        meant[i] = np.mean(xt)
        trhat[i] = irhat[0]
        xtau = (stanFit['tau'])
        meantau[i] = np.mean(xtau)
        taurhat[i] = irhat[1]

    
    if emiles.itypef== "8" or emiles.itypef=="9":
        xrv = np.zeros(len(stanFit['Rv']))
        for j in range(len(stanFit['Rv'])):
            xf = stanFit['Rv'][j]
            bf = int(xf) - 1
            xrv[j] = emiles.Rv[bf] + (xf - 1. - bf) * (emiles.Rv[bf + 1] - emiles.Rv[bf])
        Rv[i] = np.mean(xrv)
        sdrv[i] = np.std(xfehs, ddof=1)
        #Rv[i]=np.mean(stanFit['Rv'])
        RvRhat[i]=irhat[3]
        erry[i]=np.mean(stanFit['erry'])
        erryrhat[i]=irhat[4]

    elif emiles.itypef ==  "1" or emiles.itypef == "2":
        if emiles.itypemod == "1" or emiles.itypemod == "3":
            xages = (stanFit['age'] - 1.) * emiles.stepage + emiles.minage
        elif emiles.itypemod == "2":
            print("not implemented yet ")
        elif emiles.itypemod == "4" or emiles.itypemod == "5":
            xages = np.zeros(len(stanFit['age']))
            for j in range(len(stanFit['age'])):
                xf = stanFit['age'][j]
                bf = int(xf) - 1
                xages[j] = emiles.lages[bf] + (xf - 1. - bf) * (emiles.lages[bf + 1] - emiles.lages[bf])
        meanage[i] = np.mean(xages)
        sdage[i] = np.std(xages, ddof=1)
        agerhat[i]=irhat[0]
        #hdiage[i] = HDIofMCMC(xages, credMass=0.95)
                        
f = open("z21typef8otherred.txt",'w')
if emiles.itypef=="2" :
    f.write("meanfeh sdfeh fehrhat meanage sdage agerhat \n")
    for i in range(N):
        f.writelines(['%.5f' % meanfeh[i]," ",'%.5f' % sdfeh[i]," ",'%.5f' % fehrhat[i]," ",'%.5f' % meanage[i]," ",'%.5f' % sdage[i]," ",'%.5f' % agerhat[i],"\n"])
    f.close()
elif emiles.itypef=="8":
    f.write("meanfeh sdfeh fehrhat meanage sdage meanmus musrhat meantheta thetarhat meanxa meanxb Rv RvRhat erry erryRhat \n")
    for i in range(N):
        f.writelines(['%.5f' % meanfeh[i]," ",'%.5f' % sdfeh[i]," ",'%.5f' % fehrhat[i]," ",'%.5f' % meanage[i]," ",'%.5f' % sdage[i]," ",'%.5f' % meanmus[i]," ",'%.5f' % musrhat[i]," ",'%.5f' % meantheta[i] ," ",'%.5f' % thetarhat[i]," ",'%.5f' % xa[i]," ",'%.5f' % xb[i]," ",'%.5f' % Rv[i]," ",'%.5f'% sdrv[i]," ",'%.5f' % RvRhat[i]," ",'.5%f'% erry[i], " ",'%.5f' % erryrhat[i], "\n" ] )
    f.close()
elif emiles.itypef=="9":
    f.write("meanfeh sdfeh fehrhat meant trhat meantau Rv RvRhat erry erryRhat \n")
    for i in range(N):
        f.writelines(['%.5f' % meanfeh[i]," ",'%.5f' % sdfeh[i]," ",'%.5f' % fehrhat[i]," ",'%.5f' % meant[i]," ",'%.5f' % trhat[i]," ",'%.5f' % meantau[i] ," ",'%.5f' % taurhat[i]," ",'%.5f' % Rv[i]," ",'%.5f'% sdrv[i]," ",'%.5f' % RvRhat[i], " ",'%.5f' % erry[i], " ",'%.5f' % erryrhat[i], "\n" ] )
    f.close()
with open('z21typef8otherredfv.txt','wb') as f:
    for i in range(N) :
        for j in range(emiles.infilt):
            f.writelines(['%.5f' % Results[i,j]," "])
        f.writelines(["\n "])


# 0.00. plt.plot(emiles.slambdab, emiles.modelsb[1, ])
# emiles.filter("Challenge-master/filters.dat") #file with the names of the filters alredy in the library
# metalicidad=np.zeros((emiles.nfehs,emiles.infilt))
# for i in range(emiles.nfehs):
#    metalicidad[i,]=emiles.modelsb[i*emiles.nages+emiles.nages-10,]
# plt.loglog(emiles.lages,emiles.modelsb[8*emiles.nages:9*emiles.nages, ])
# plt.plot(emiles.lfehs,metalicidad)
# plt.show()
# emiles.save_spectrum_as_fits("Phot_JPAS_Emiles/")
# emiles.run_stan2(dataforstan, modelfile, fitfile, stanparameters,"R")
